﻿namespace PublicTransport.Logic
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using PublicTransport.Data.Models;

    /// <summary>
    /// Define methods for a HumanResourceLogic.
    /// </summary>
    public interface IHumanResourceLogic
    {
        /// <summary>
        /// Add a new Employee to the repository.
        /// </summary>
        /// <param name="employee">The Employee object, filled with data.</param>
        void NewEmployee(Employee employee);

        /// <summary>
        /// Remove an Employee from the repository.
        /// </summary>
        /// <param name="employeeId">The ID of the employee you want to delete. </param>
        /// <returns>Returns true if the removeing was successful, returns false if not.</returns>
        bool RemoveEmployee(string employeeId);

        /// <summary>
        /// Finds an employee in the repository, based on the name of the employee.
        /// </summary>
        /// <param name="name">The name of the employee in the following form: 'FirsName LastName'.</param>
        /// <returns>Returns the specified employee from the EmployeeRepository, as an entity or null if not found.</returns>
        Employee GetByName(string name);

        /// <summary>
        /// Finds an employee in the repository, based on the ID of the employee.
        /// </summary>
        /// <param name="employeeId"> The ID of the employee.</param>
        /// <returns>Returns the specified employee from the EmployeeRepository, as an entity or null if not found.</returns>
        Employee GetById(string employeeId);

        /// <summary>
        /// Returns the all employee from the repository as a list.
        /// </summary>
        /// <returns>An employee list.</returns>
        IList<Employee> GetAllEmployee();

        /// <summary>
        /// Changes the name of a specified employee.
        /// </summary>
        /// <param name="employeeId">The ID of the employee.</param>
        /// <param name="name">The new name.</param>
        void ChangeName(string employeeId, string name);

        /// <summary>
        /// Changes the position of a specified employee.
        /// </summary>
        /// <param name="employeeId">The ID of the employee.</param>
        /// <param name="position">The new positon of the employee.</param>
        void ChangePositon(string employeeId, string position);

        /// <summary>
        /// Changes the wage of a specified employee.
        /// </summary>
        /// <param name="employeeId">The ID of the employee.</param>
        /// <param name="wage">The new positon of the employee.</param>
        void ChangeWage(string employeeId, int wage);

        /// <summary>
        /// Returns the all employee who is in the given position.
        /// </summary>
        /// <param name="position">The positon sought.</param>
        /// <returns>An employee list.</returns>
        IList<Employee> GetByPosition(string position);

        /// <summary>
        /// Returns the all employee, who has a birthday on a given day.
        /// </summary>
        /// <param name="dateT">The date as a DateTime.</param>
        /// <returns>An employee list.</returns>
        IList<Employee> GetBirthdays(DateTime dateT);

        /// <summary>
        /// Returns the all employee, who has worked for the company for less than a year.
        /// </summary>
        /// <returns>An employee list.</returns>
        IList<Employee> GetBeginers();
    }
}
