﻿namespace PublicTransport.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using PublicTransport.Data.Models;
    using PublicTransport.Repository;

    /// <summary>
    /// This class made for the human resource management department.
    /// </summary>
    public class HumanResourceLogic : IHumanResourceLogic
    {
        private IEmployeeRepository employeeRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="HumanResourceLogic"/> class.
        /// </summary>
        /// <param name="employeeRepository">A repository which stores Employee objects and implemets the IEmployeeRepository interface.</param>
        public HumanResourceLogic(IEmployeeRepository employeeRepository)
        {
            this.employeeRepository = employeeRepository;
        }

        /// <inheritdoc/>
        public void ChangeName(string employeeId, string name)
        {
            employeeRepository.UpdateName(employeeId, name);
        }

        /// <inheritdoc/>
        public void ChangePositon(string employeeId, string position)
        {
            employeeRepository.UpdateType(employeeId, position);
        }

        /// <inheritdoc/>
        public void ChangeWage(string employeeId, int wage)
        {
            employeeRepository.UpdateWage(employeeId, wage);
        }

        /// <inheritdoc/>
        public IList<Employee> GetAllEmployee()
        {
            return employeeRepository.GetAll().ToList();
        }

        /// <inheritdoc/>
        public IList<Employee> GetBeginers()
        {
            var result = from employee in employeeRepository.GetAll()
                         where employee.HireDate.AddDays(365) > DateTime.Now
                         select employee;
            return result.ToList();
        }

        /// <inheritdoc/>
        public IList<Employee> GetBirthdays(DateTime dateT)
        {
            var result = from employee in employeeRepository.GetAll()
                         where employee.DateOfBirth.Month == dateT.Month && employee.DateOfBirth.Day == dateT.Day
                         select employee;
            return result.ToList();
        }

        /// <inheritdoc/>
        public Employee GetById(string employeeId)
        {
            return employeeRepository.GetOne(employeeId);
        }

        /// <inheritdoc/>
        public Employee GetByName(string name)
        {
            var result = from employee in employeeRepository.GetAll()
                         where employee.Name == name
                         select employee;
            return result.FirstOrDefault();
        }

        /// <inheritdoc/>
        public IList<Employee> GetByPosition(string position)
        {
            var result = from employee in employeeRepository.GetAll()
                         where employee.Type == position
                         select employee;
            return result.ToList();
        }

        /// <inheritdoc/>
        public void NewEmployee(Employee employee)
        {
            employeeRepository.NewRecord(employee);
        }

        /// <inheritdoc/>
        public bool RemoveEmployee(string employeeId)
        {
            return employeeRepository.DeleteOne(employeeId);
        }
    }
}
