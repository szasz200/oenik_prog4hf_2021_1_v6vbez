﻿namespace PublicTransport.Logic
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Represents an employee - worked hours  pair.
    /// </summary>
    public class WorkHours
    {
        /// <summary>
        /// Gets or sets the EmpolyeeID.
        /// </summary>
        public string EmpolyeeID { get; set; }

        /// <summary>
        /// Gets or sets the EmpoloyeeName.
        /// </summary>
        public string EmpoloyeeName { get; set; }

        /// <summary>
        /// Gets or sets the WorkedHours.
        /// </summary>
        public decimal WorkedHours { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            return obj is WorkHours hours &&
                   EmpolyeeID == hours.EmpolyeeID &&
                   EmpoloyeeName == hours.EmpoloyeeName &&
                   WorkedHours == hours.WorkedHours;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return HashCode.Combine(EmpolyeeID, EmpoloyeeName, WorkedHours);
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return EmpolyeeID + "\t" + EmpoloyeeName + "\t" + WorkedHours.ToString(CultureInfo.CurrentCulture);
        }
    }
}
