﻿namespace PublicTransport.Logic
{
    using System.Globalization;
    using PublicTransport.Repository;

    /// <summary>
    /// Stores a Type and AvarageTravelDistance pair.
    /// </summary>
    public class TypeAvarageTD
    {
        /// <summary>
        /// Gets or sets the vehicle Type.
        /// </summary>
        public VType Type { get; set; }

        /// <summary>
        /// Gets or sets the AvarageVehicleAge.
        /// </summary>
        public decimal AvarageTravelDistance { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is TypeAvarageTD)
            {
                TypeAvarageTD o = (TypeAvarageTD)obj;
                return o.AvarageTravelDistance == this.AvarageTravelDistance && o.Type == this.Type;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return (int)AvarageTravelDistance * Type.GetHashCode();
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return Type.ToString() + "\t" + AvarageTravelDistance.ToString(CultureInfo.CurrentCulture);
        }
    }
}
