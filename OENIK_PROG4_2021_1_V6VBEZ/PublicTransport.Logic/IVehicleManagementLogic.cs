﻿namespace PublicTransport.Logic
{
    using System;
    using System.Collections.Generic;
    using PublicTransport.Data.Models;

    /// <summary>
    /// Define methods for a VehicleManagement.
    /// </summary>
    internal interface IVehicleManagementLogic
    {
        /// <summary>
        /// Returns a from the repository.
        /// </summary>
        /// <param name="lpnumber">The license plate number of the vehicle you want to remove.</param>
        /// <returns>A Vehicle object.</returns>
        public Vehicle GetVehicle(string lpnumber);

        /// <summary>
        /// Changes the next maintenance date of a specified vehicle.
        /// </summary>
        /// <param name="lpnumber">The license plate number of the vehicle.</param>
        /// <param name="newDateTime">The new date of the next maintenance.</param>
        public void ChangeNextMaintenance(string lpnumber, DateTime newDateTime);

        /// <summary>
        /// Add a new Vehicle to the repository.
        /// </summary>
        /// <param name="vehicle">The Vehicle object, filled with data.</param>
        void NewVehicle(Vehicle vehicle);

        /// <summary>
        /// Add a new NewMaintenance to the repository.
        /// </summary>
        /// <param name="maintenance">The Maintenance object, filled with data.</param>
        void NewMaintenance(Maintenance maintenance);

        /// <summary>
        /// Remove a Vehicle from the repository.
        /// </summary>
        /// <param name="lpnumber">The license plate number of the vehicle you want to remove.</param>
        void RemoveVehicle(string lpnumber);

        /// <summary>
        /// Returns the all type of vehicles, with the avarage TraveledDistances.
        /// </summary>
        /// <returns>The list of Type-AvarageTraveledDistances pairs.</returns>
        IList<TypeAvarageTD> GetAvarageTypeTDistances();

        /// <summary>
        /// Returns the all type of vehicles, with the avarage age of vehicles.
        /// </summary>
        /// <returns>The list of Type-AvarageVehicleAge pairs.</returns>
        IList<TypeAvarageA> GetAvarageVehicleTypeAge();

        /// <summary>
        /// Returns the all vehicles with, the NextMaintenance date.
        /// </summary>
        /// <returns>A list with vehicle license plate number - NextMaintenance pairs.</returns>
        IList<VehicleNextMaintances> GetNextMaintances();

        /// <summary>
        /// Returns the maintenances of a vehicle.
        /// </summary>
        /// <param name="lpnumber">The license plate number of the vehicle you want to remove.</param>
        /// <returns>The maintenance list of the vehicle.</returns>
        IList<Maintenance> GetMaintenances(string lpnumber);

        /// <summary>
        /// Returns the all vehicle which must be maintained on the specified date.
        /// </summary>
        /// <param name="date">A date as DateTime.</param>
        /// <returns>The list of the vehicles which must be maintained on the specified date.</returns>
        IList<Vehicle> GetTodayMaintenances(DateTime date);

        /// <summary>
        /// Returns the all vehicle from the repository.
        /// </summary>
        /// <returns>A list with the all Vehicles objects.</returns>
        IList<Vehicle> Vehicles();
    }
}
