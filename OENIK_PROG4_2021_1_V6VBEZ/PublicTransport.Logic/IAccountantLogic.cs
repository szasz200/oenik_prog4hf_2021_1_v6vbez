﻿namespace PublicTransport.Logic
{
    using System;
    using System.Collections.Generic;
    using PublicTransport.Data.Models;

    /// <summary>
    /// Define methods for a AccountantLogic.
    /// </summary>
    internal interface IAccountantLogic
    {
        /// <summary>
        /// Add a new working day to the repository.
        /// </summary>
        /// <param name="workingDate">The WorkingDate object, filled with data.</param>
        void NewWorkDate(WorkingDate workingDate);

        /// <summary>
        /// Return the wage of an employee.
        /// </summary>
        /// <param name="employeeID">The ID of the employee.</param>
        /// <returns>The wage of the empolyee.</returns>
        int GetWageEmployee(string employeeID);

        /// <summary>
        /// Returns the sum of consumption of buses in a month.
        /// </summary>
        /// <param name="month">The number of the given month.</param>
        /// <returns>Amount of fuel consumed.(Liters).</returns>
        int GetDieselConsumption(int month);

        /// <summary>
        /// Returs the sum of consumption of trolleies and tram, in a month.
        /// </summary>
        /// <param name="month">The number of the given month.</param>
        /// <returns>Amount of fuel consumed.(KWh).</returns>
        int GetElectricityConsumption(int month);

        /// <summary>
        /// Returns the all vehicle, with they maitenance cost between two dates.
        /// </summary>
        /// <param name="f">The earlier date parameter.</param>
        /// <param name="to">The later date parameter.</param>
        /// <returns>The list of vehicle - cost pairs.</returns>
        IList<VehicleCost> GetVehicleCosts(DateTime f, DateTime to);

        /// <summary>
        /// Returns the the number of days worked the given month.
        /// </summary>
        /// <param name="employeeID">The ID of the employee.</param>
        /// <param name="month">The number of the given month.</param>
        /// <returns>The number of worked days.</returns>
        int GetEmployeeWorkingDates(string employeeID, int month);

        /// <summary>
        /// Returns the all employee working hours of the given month.
        /// </summary>
        /// <param name="month">The number of the given month.</param>
        /// <returns>Returns a list with employee-worked hours pairs.</returns>
        IList<WorkHours> GetEmployeeWorkingMinutes(int month);

        /// <summary>
        /// Returns the all WorkingDate from the repositorie.
        /// </summary>
        /// <returns>A list with WorikingDates entities. </returns>
        IList<WorkingDate> GetWorkingDates();

        /// <summary>
        /// Returns the all Maintenance from the repositorie.
        /// </summary>
        /// <returns>A list with Maintenance entities. </returns>
        IList<Maintenance> GetMaintenances();
    }
}
