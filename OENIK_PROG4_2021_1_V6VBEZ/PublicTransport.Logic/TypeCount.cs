﻿namespace PublicTransport.Logic
{
    using System;
    using System.Globalization;
    using PublicTransport.Repository;

    /// <summary>
    /// Represents a linetype - linecount pair.
    /// </summary>
    public class TypeCount
    {
        /// <summary>
        /// Gets or sets the type of the line.
        /// </summary>
        public VType Type { get; set; }

        /// <summary>
        /// Gets or sets the count of lines.
        /// </summary>
        public int Count { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            return obj is TypeCount count &&
                   Type == count.Type &&
                   Count == count.Count;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return HashCode.Combine(Type, Count);
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return Type.ToString() + "\t" + Count.ToString(CultureInfo.InvariantCulture);
        }
    }
}