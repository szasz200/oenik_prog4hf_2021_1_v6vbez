﻿namespace PublicTransport.Logic
{
    /// <summary>
    /// Represents a license plate number - Cost pair.
    /// </summary>
    public class VehicleCost
    {
        /// <summary>
        /// Gets or sets the license plate number.
        /// </summary>
        public string Lpnumber { get; set; }

        /// <summary>
        /// Gets or sets the cost.
        /// </summary>
        public int Cost { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is VehicleCost)
            {
                VehicleCost o = (VehicleCost)obj;
                return o.Lpnumber == this.Lpnumber && o.Cost == this.Cost;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return Lpnumber[1] + Lpnumber[2] + Cost;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"LPNumber: {Lpnumber}   Cost: {Cost}";
        }
    }
}
