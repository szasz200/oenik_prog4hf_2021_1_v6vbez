﻿namespace PublicTransport.Logic
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Stores a LPNumber and NextMaintenanceDate pair.
    /// </summary>
    public class VehicleNextMaintances
    {
        /// <summary>
        /// Gets or sets the lincense plate number of vehicle.
        /// </summary>
        public string LPNumber { get; set; }

        /// <summary>
        /// Gets or sets the NextMaintenanceDate of a vehicle.
        /// </summary>
        public DateTime NextMaintenanceDate { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return LPNumber + "\t" + NextMaintenanceDate.ToString(CultureInfo.CurrentCulture);
        }
    }
}
