﻿namespace PublicTransport.Logic
{
    using System.Collections.Generic;
    using PublicTransport.Data.Models;

    /// <summary>
    /// Define methods for a NetworkManager.
    /// </summary>
    internal interface INetworkManagementLogic
    {
        /// <summary>
        /// Add a new line to the repository.
        /// </summary>
        /// <param name="line">The Line object, filled with data.</param>
        void NewLine(Line line);

        /// <summary>
        /// Returns the all line from the repository.
        /// </summary>
        /// <returns>A list with the all Line objects.</returns>
        IList<Line> GetLines();

        /// <summary>
        /// Returns a Line object, with similar LineID.
        /// </summary>
        /// <param name="lineId">The ID of the line.</param>
        /// <returns>The Line object.</returns>
        Line GetLine(string lineId);

        /// <summary>
        /// Returns the sum of the lines length.
        /// </summary>
        /// <returns>The sum of the lines length.</returns>
        int LengthOfNetwork();

        /// <summary>
        /// Returns the count of lines by types.
        /// </summary>
        /// <returns>The list of type-count pairs.</returns>
        IList<TypeCount> GetTypeCounts();

        /// <summary>
        /// Changes the length of a specified line.
        /// </summary>
        /// <param name="lineId">The ID of the line.</param>
        /// <param name="length">The new length.</param>
        void ChangeLength(string lineId, decimal length);

        /// <summary>
        /// Changes the TimeRequired of a specified line.
        /// </summary>
        /// <param name="lineId">The ID of the line.</param>
        /// <param name="timeRequired">The new TimeRequired.</param>
        void ChangeTimeRequired(string lineId, int timeRequired);

        /// <summary>
        /// Changes the NumberOfStops of a specified line.
        /// </summary>
        /// <param name="lineId">The ID of the line.</param>
        /// <param name="numberOfStops">The new NumberOfStops.</param>
        void ChangeNumberOfStops(string lineId, int numberOfStops);

        /// <summary>
        /// Changes the NumberOfVehicles of a specified line.
        /// </summary>
        /// <param name="lineId">The ID of the line.</param>
        /// <param name="numberOfVehicles">The new NumberOfVehicles.</param>
        void ChangeNumberOfVehicles(string lineId, int numberOfVehicles);

        /// <summary>
        /// Returs the all LineType, with the avarage line length of the type.
        /// </summary>
        /// <returns>The line type - avarage length pairs as TypeAvarageLenth list.</returns>
        IList<TypeAvarageLength> AvarageLineTypeLengths();
    }
}
