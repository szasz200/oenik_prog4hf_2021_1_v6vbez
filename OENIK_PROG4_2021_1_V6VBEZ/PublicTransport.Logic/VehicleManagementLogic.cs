﻿namespace PublicTransport.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using PublicTransport.Data.Models;
    using PublicTransport.Repository;

    /// <summary>
    /// This class made for the vehicle management department.
    /// </summary>
    public class VehicleManagementLogic : IVehicleManagementLogic
    {
        private IVehicleRepository vehicleRepository;
        private IMaintenanceRepository maintenanceRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="VehicleManagementLogic"/> class.
        /// </summary>
        /// <param name="vehicleRepository">A repository which stores Vehicle objects and implemets the IVehicleRepository interface.</param>
        /// <param name="maintenanceRepository">A repository which stores Maintenance objects and implemets the IMaintenanceRepository interface.</param>
        public VehicleManagementLogic(IVehicleRepository vehicleRepository, IMaintenanceRepository maintenanceRepository)
        {
            this.vehicleRepository = vehicleRepository;
            this.maintenanceRepository = maintenanceRepository;
        }

        /// <inheritdoc/>
        public Vehicle GetVehicle(string lpnumber)
        {
            return vehicleRepository.GetOne(lpnumber);
        }

        /// <inheritdoc/>
        public IList<TypeAvarageTD> GetAvarageTypeTDistances()
        {
            var result = from vehicle in vehicleRepository.GetAll()
                         group vehicle by vehicle.Type into grp
                         select new TypeAvarageTD { Type = Enum.Parse<VType>(grp.Key), AvarageTravelDistance = grp.Average(x => x.TraveledDistance) };
            return result.ToList();
        }

        /// <inheritdoc/>
        public IList<TypeAvarageA> GetAvarageVehicleTypeAge()
        {
            var result = from vehicle in vehicleRepository.GetAll()
                         group vehicle by vehicle.Type into grp
                         select new TypeAvarageA { Type = Enum.Parse<VType>(grp.Key), AvarageVehicleAge = (int)grp.Average(x => DateTime.Today.Year - x.YearOfManufacture) };
            return result.ToList();
        }

        /// <inheritdoc/>
        public IList<Maintenance> GetMaintenances(string lpnumber)
        {
            return maintenanceRepository.GetAll().Where(x => x.Lpnumber == lpnumber).ToList();
        }

        /// <inheritdoc/>
        public IList<VehicleNextMaintances> GetNextMaintances()
        {
            var result = from vehicle in vehicleRepository.GetAll()
                         where vehicle.NextMaintenance != null
                         select new VehicleNextMaintances { LPNumber = vehicle.Lpnumber, NextMaintenanceDate = vehicle.NextMaintenance.GetValueOrDefault() };
            return result.ToList();
        }

        /// <inheritdoc/>
        public IList<Vehicle> GetTodayMaintenances(DateTime date)
        {
            var result = from vehicle in vehicleRepository.GetAll()
                         where vehicle.NextMaintenance == date
                         select vehicle;
            return result.ToList();
        }

        /// <inheritdoc/>
        public void NewMaintenance(Maintenance maintenance)
        {
            maintenanceRepository.NewRecord(maintenance);
            vehicleRepository.UpdateLastMaintenanceId(maintenance?.Lpnumber, maintenance.MaintenanceId);
        }

        /// <inheritdoc/>
        public void NewVehicle(Vehicle vehicle)
        {
            vehicleRepository.NewRecord(vehicle);
        }

        /// <inheritdoc/>
        public void RemoveVehicle(string lpnumber)
        {
            vehicleRepository.DeleteOne(lpnumber);
        }

        /// <inheritdoc/>
        public IList<Vehicle> Vehicles()
        {
            return vehicleRepository.GetAll().ToList();
        }

        /// <inheritdoc/>
        public void ChangeNextMaintenance(string lpnumber, DateTime newDateTime)
        {
            vehicleRepository.UpdateNextMaintenance(lpnumber, newDateTime);
        }
    }
}
