﻿namespace PublicTransport.Logic
{
    using System.Globalization;
    using PublicTransport.Repository;

    /// <summary>
    /// Stores a Type and AvarageVehicleAge pair.
    /// </summary>
    public class TypeAvarageA
    {
        /// <summary>
        /// Gets or sets the vehicle Type.
        /// </summary>
        public VType Type { get; set; }

        /// <summary>
        /// Gets or sets the AvarageVehicleAge.
        /// </summary>
        public decimal AvarageVehicleAge { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is TypeAvarageA)
            {
                TypeAvarageA o = (TypeAvarageA)obj;
                return o.AvarageVehicleAge == this.AvarageVehicleAge && o.Type == this.Type;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return (int)AvarageVehicleAge * Type.GetHashCode();
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return Type.ToString() + "\t" + AvarageVehicleAge.ToString(CultureInfo.InvariantCulture);
        }
    }
}
