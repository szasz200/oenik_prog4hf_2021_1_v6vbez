﻿namespace PublicTransport.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography.X509Certificates;
    using System.Security.Principal;
    using System.Threading.Tasks;
    using PublicTransport.Data.Models;
    using PublicTransport.Repository;

    /// <summary>
    /// This class made for the accountant department.
    /// </summary>
    public class AccountantLogic : IAccountantLogic
    {
        private IWorkingDateRepository workingDateRepository;
        private IMaintenanceRepository maintenanceRepository;
        private IEmployeeRepository employeeRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountantLogic"/> class.
        /// </summary>
        /// <param name="workingDateRepository">A repository which stores WorkingDate objects and implemets the IWorkingDateRepository interface.</param>
        /// <param name="maintenanceRepository">A repository which stores Maintenance objects and implemets the IMaintenanceRepository interface.</param>
        /// <param name="employeeRepository">A repository which stores Employee objects and implemets the IEmployeeRepository interface.</param>
        public AccountantLogic(IWorkingDateRepository workingDateRepository, IMaintenanceRepository maintenanceRepository, IEmployeeRepository employeeRepository)
        {
            this.workingDateRepository = workingDateRepository;
            this.maintenanceRepository = maintenanceRepository;
            this.employeeRepository = employeeRepository;
        }

        /// <inheritdoc/>
        public int GetDieselConsumption(int month)
        {
            int year;
            if (month > DateTime.Today.Month)
            {
                year = DateTime.Today.Year - 1;
            }
            else
            {
                year = DateTime.Today.Year;
            }

            var consumption = from workdate in workingDateRepository.GetAll()
                              join employee in employeeRepository.GetAll() on workdate.EmployeeId equals employee.EmployeeId
                              where workdate.Date.Year == year && workdate.Date.Month == month && employee.Type == "Bus driver"
                              select workdate.Consumption;
            return consumption.Sum();
        }

        /// <summary>
        /// This method is similar to the GetDieselConsumption method, but it runs asynchronously.
        /// (Returns the sum of consumption of buses in a month.)
        /// </summary>
        /// <param name="month">The number of the given month.</param>
        /// <returns>Returns a Task with the result.</returns>
        public Task<int> GetDieselConsumptionAsync(int month)
        {
            return Task<int>.Factory.StartNew(() => GetDieselConsumption(month));
        }

        /// <inheritdoc/>
        public int GetElectricityConsumption(int month)
        {
            int year;
            if (month > DateTime.Today.Month)
            {
                year = DateTime.Today.Year - 1;
            }
            else
            {
                year = DateTime.Today.Year;
            }

            var consumption = from workdate in workingDateRepository.GetAll()
                              join employee in employeeRepository.GetAll() on workdate.EmployeeId equals employee.EmployeeId
                              where workdate.Date.Year == year && workdate.Date.Month == month && (employee.Type == "Tram driver" || employee.Type == "Trolley driver")
                              select workdate.Consumption;
            return consumption.Sum();
        }

        /// <summary>
        /// This method is similar to the GetElectricityConsumption method, but it runs asynchronously.
        /// (Returs the sum of consumption of trolleies and tram, in a month.)
        /// </summary>
        /// <param name="month">The number of the given month.</param>
        /// <returns>Returns a Task with the result.</returns>
        public Task<int> GetElectricityConsumptionAsync(int month)
        {
            return Task<int>.Factory.StartNew(() => GetElectricityConsumption(month));
        }

        /// <inheritdoc/>
        public int GetEmployeeWorkingDates(string employeeID, int month)
        {
            var result = from workday in workingDateRepository.GetAll()
                         where workday.Date.Month == month && workday.EmployeeId == employeeID
                         select workday;
            return result.Count();
        }

        /// <inheritdoc/>
        public IList<WorkHours> GetEmployeeWorkingMinutes(int month)
        {
            var result = from employee in employeeRepository.GetAll()
                         join workday in workingDateRepository.GetAll() on employee.EmployeeId equals workday.EmployeeId
                         where workday.Date.Month == month
                         select new { name = employee.Name, id = workday.EmployeeId, minutes = workday.WorkingMinutes };
            var result2 = from employee in result.ToList()
                          group employee by employee.id into grp
                          select new WorkHours { EmpolyeeID = grp.Key, EmpoloyeeName = grp.FirstOrDefault().name, WorkedHours = grp.Sum(x => x.minutes) / 60 };
            return result2.ToList();
        }

        /// <summary>
        /// This method is similar to the GetEmployeeWorkingMinutes method, but it runs asynchronously.
        /// (Returns the all employee working hours of the given month.)
        /// </summary>
        /// <param name="month">The number of the given month.</param>
        /// <returns>Returns a Task with result list with employee-worked hours pairs.</returns>
        public Task<IList<WorkHours>> GetEmployeeWorkingMinutesAsync(int month)
        {
            return Task<IList<WorkHours>>.Factory.StartNew(() => GetEmployeeWorkingMinutes(month));
        }

        /// <inheritdoc/>
        public IList<Maintenance> GetMaintenances()
        {
            return maintenanceRepository.GetAll().ToList();
        }

        /// <inheritdoc/>
        public IList<VehicleCost> GetVehicleCosts(DateTime f, DateTime to)
        {
            var result = from maintenance in maintenanceRepository.GetAll()
                         where maintenance.Date.CompareTo(f) >= 0 && maintenance.Date.CompareTo(to) <= 0
                         group maintenance by maintenance.Lpnumber into grp
                         select new VehicleCost { Lpnumber = grp.Key, Cost = grp.Sum(x => x.Cost.GetValueOrDefault()) };
            return result.ToList();
        }

        /// <inheritdoc/>
        public int GetWageEmployee(string employeeID)
        {
            return employeeRepository.GetOne(employeeID).Wage;
        }

        /// <inheritdoc/>
        public IList<WorkingDate> GetWorkingDates()
        {
            return workingDateRepository.GetAll().ToList();
        }

        /// <inheritdoc/>
        public void NewWorkDate(WorkingDate workingDate)
        {
            workingDateRepository.NewRecord(workingDate);
        }
    }
}
