﻿namespace PublicTransport.Logic
{
    using System;
    using System.Globalization;
    using PublicTransport.Repository;

    /// <summary>
    /// Stores a LineType - avarage length pair.
    /// </summary>
    public class TypeAvarageLength : IEquatable<TypeAvarageLength>
    {
        /// <summary>
        /// Gets or sets the type of the Line.
        /// </summary>
        public VType LineType { get; set; }

        /// <summary>
        /// Gets or sets the avarage length of the line type.
        /// </summary>
        public decimal LineTypeLength { get; set; }

        /// <inheritdoc/>
        public bool Equals(TypeAvarageLength other)
        {
            return other != null &&
                   LineType == other.LineType &&
                   LineTypeLength == other.LineTypeLength;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return HashCode.Combine(LineType, LineTypeLength);
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is TypeAvarageLength)
            {
                TypeAvarageLength other = (TypeAvarageLength)obj;
                return other != null &&
                        LineType == other.LineType &&
                        LineTypeLength == other.LineTypeLength;
            }

            return false;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return LineType.ToString() + "\t" + LineTypeLength.ToString(CultureInfo.InvariantCulture);
        }
    }
}
