﻿namespace PublicTransport.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using PublicTransport.Data.Models;
    using PublicTransport.Repository;

    /// <summary>
    /// This class made for the network management department.
    /// </summary>
    public class NetworkManagementLogic : INetworkManagementLogic
    {
        private ILineRepository lineRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkManagementLogic"/> class.
        /// </summary>
        /// <param name="lineRepository">A repository which stores Line objects and implemets the ILineRepository interface.</param>
        public NetworkManagementLogic(ILineRepository lineRepository)
        {
            this.lineRepository = lineRepository;
        }

        /// <inheritdoc/>
        public IList<TypeAvarageLength> AvarageLineTypeLengths()
        {
            var result = from line in lineRepository.GetAll()
                         group line by line.Type into grp
                         select new TypeAvarageLength { LineType = Enum.Parse<VType>(grp.Key), LineTypeLength = grp.Average(x => x.Length) };
            return result.ToList();
        }

        /// <summary>
        /// This method is similar to the AvarageLineTypeLengths method, but it runs asynchronously.
        /// (Returs the all LineType, with the avarage line length of the type.)
        /// </summary>
        /// <returns>Returns a Task with the resul list.</returns>
        public IList<TypeAvarageLength> AvarageLineTypeLengthsAsync()
        {
            return Task<IList<TypeAvarageLength>>.Factory.StartNew(() => AvarageLineTypeLengths()).Result;
        }

        /// <inheritdoc/>
        public void ChangeLength(string lineId, decimal length)
        {
            lineRepository.UpdateLength(lineId, length);
        }

        /// <inheritdoc/>
        public void ChangeNumberOfStops(string lineId, int numberOfStops)
        {
            lineRepository.UpdateNumberOfStops(lineId, numberOfStops);
        }

        /// <inheritdoc/>
        public void ChangeNumberOfVehicles(string lineId, int numberOfVehicles)
        {
            lineRepository.UpdateNumberOfVehicles(lineId, numberOfVehicles);
        }

        /// <inheritdoc/>
        public void ChangeTimeRequired(string lineId, int timeRequired)
        {
            lineRepository.UpdateTimeRequired(lineId, timeRequired);
        }

        /// <inheritdoc/>
        public Line GetLine(string lineId)
        {
            return lineRepository.GetOne(lineId);
        }

        /// <inheritdoc/>
        public IList<Line> GetLines()
        {
            return lineRepository.GetAll().ToList();
        }

        /// <inheritdoc/>
        public IList<TypeCount> GetTypeCounts()
        {
            var result = from line in lineRepository.GetAll()
                         group line by line.Type into grp
                         select new TypeCount { Type = Enum.Parse<VType>(grp.Key), Count = grp.Count() };
            return result.ToList();
        }

        /// <inheritdoc/>
        public int LengthOfNetwork()
        {
            return (int)lineRepository.GetAll().Sum(x => x.Length);
        }

        /// <inheritdoc/>
        public void NewLine(Line line)
        {
            lineRepository.NewRecord(line);
        }
    }
}
