﻿namespace PublicTransportWPF.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using PublicTransportWPF.Data;
    using PublicTransportWPF.VM;

    /// <summary>
    /// Interaction logic for EmpolyeeEditor.xaml.
    /// </summary>
    public partial class EmpolyeeEditor : Window
    {
        private EmployeeEditorViewModel vM;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmpolyeeEditor"/> class.
        /// </summary>
        public EmpolyeeEditor()
        {
            InitializeComponent();
            vM = FindResource("VM") as EmployeeEditorViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmpolyeeEditor"/> class.
        /// </summary>
        /// <param name="oldEmployee">The old employee object.</param>
        /// <param name="newEmployee">True if this is a new employee.</param>
        public EmpolyeeEditor(Employee oldEmployee, bool newEmployee)
            : this()
        {
            vM.Employee = oldEmployee;
            vM.IsAdd = newEmployee;
        }

        /// <summary>
        /// Gets the empoloyee entity from the ViewModel.
        /// </summary>
        public Employee Employee { get => vM.Employee; }

        private void OkClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
