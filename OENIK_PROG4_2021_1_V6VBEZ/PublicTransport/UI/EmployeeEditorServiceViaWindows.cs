﻿namespace PublicTransportWPF.UI
{
    using PublicTransportWPF.BL;
    using PublicTransportWPF.Data;

    /// <summary>
    /// Implemetation of IEditorService interface (only for employee).
    /// </summary>
    public class EmployeeEditorServiceViaWindows : IEditorService
    {
        /// <inheritdoc/>
        public bool EditEntity(IPublicTransportEntity entity, bool newEntity)
        {
            EmpolyeeEditor win = new EmpolyeeEditor(entity as Employee, newEntity);
            return win.ShowDialog() ?? false;
        }
    }
}
