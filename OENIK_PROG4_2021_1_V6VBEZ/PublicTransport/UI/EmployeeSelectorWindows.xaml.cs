﻿namespace PublicTransportWPF.UI
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using PublicTransportWPF.VM;

    /// <summary>
    /// Interaction logic for EmployeeSelectorWindows.xaml.
    /// </summary>
    public partial class EmployeeSelectorWindows : Window
    {
        private EmpolyeeSelectorViewModel vM;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeSelectorWindows"/> class.
        /// </summary>
        public EmployeeSelectorWindows()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            vM = FindResource("VM") as EmpolyeeSelectorViewModel;

            Messenger.Default.Register<string>(this, "LogicResult", msg =>
            {
                MessageBox.Show(msg);
            });
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Messenger.Default.Unregister(this);
        }
    }
}
