﻿namespace PublicTransport
{
    using System.Windows;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Messaging;
    using PublicTransportWPF.BL;
    using PublicTransportWPF.Data;
    using PublicTransportWPF.UI;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => MyIoc.Instance);
            MyIoc.Instance.Register<IEditorService, EmployeeEditorServiceViaWindows>();
            MyIoc.Instance.Register<IEntityLogic, EntityLogic<Employee>>();
            MyIoc.Instance.Register<IMessenger>(() => Messenger.Default);
        }
    }
}
