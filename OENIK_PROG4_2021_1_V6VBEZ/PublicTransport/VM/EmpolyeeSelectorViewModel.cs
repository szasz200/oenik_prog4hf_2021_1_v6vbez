﻿namespace PublicTransportWPF.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using PublicTransportWPF.BL;
    using PublicTransportWPF.Data;

    /// <summary>
    /// The EmployeeSelector window ViewModel class in the MVVM pattern.
    /// </summary>
    public class EmpolyeeSelectorViewModel : ViewModelBase
    {
        private IEntityLogic logic;
        private Employee employeeSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmpolyeeSelectorViewModel"/> class.
        /// </summary>
        /// <param name="logic"> A logic instance of an entity. </param>
        public EmpolyeeSelectorViewModel(IEntityLogic logic)
        {
            this.logic = logic;
            Employees = new ObservableCollection<IPublicTransportEntity>();
            if (IsInDesignMode)
            {
                Employees.Add(new Employee() { EmployeeId = "A365B2CD", DateOfBirth = new DateTime(1985, 06, 15), HireDate = new DateTime(2019, 02, 11), Name = "Gábor Kovács", Type = JobType.BusDriver, Wage = 220000 });
                Employees.Add(new Employee() { EmployeeId = "A123B4CD", DateOfBirth = new DateTime(1987, 06, 15), HireDate = new DateTime(2020, 02, 11), Name = "Erika Kovács", Type = JobType.TramDriver, Wage = 250000 });
            }
            else
            {
                this.logic.GetAllEntity(new Employee()).ToList().ForEach(x => Employees.Add(x));
            }

            AddEmp = new RelayCommand(() => this.logic.AddEntity(Employees));
            ModEmp = new RelayCommand(() => this.logic.ModifyEntity(EmployeeSelected));
            DelEmp = new RelayCommand(() => this.logic.DeleteEntity(Employees, EmployeeSelected));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmpolyeeSelectorViewModel"/> class.
        /// </summary>
        public EmpolyeeSelectorViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IEntityLogic>())
        {
        }

        /// <summary>
        /// Gets a dynamic data collection that provides notifications when an employee get added, removed, or when the whole list is refreshed.
        /// </summary>
        public ObservableCollection<IPublicTransportEntity> Employees { get; private set; }

        /// <summary>
        /// Gets or sets the selected employee from the Employees collection.
        /// </summary>
        public Employee EmployeeSelected
        {
            get { return employeeSelected; }
            set { Set(ref employeeSelected, value); }
        }

        /// <summary>
        /// Gets the command to add an employee.
        /// </summary>
        public ICommand AddEmp { get; private set; }

        /// <summary>
        /// Gets the command to modify an employee.
        /// </summary>
        public ICommand ModEmp { get; private set; }

        /// <summary>
        /// Gets the command to delete an employee.
        /// </summary>
        public ICommand DelEmp { get; private set; }
    }
}
