﻿namespace PublicTransportWPF.VM
{
    using System;
    using GalaSoft.MvvmLight;
    using PublicTransportWPF.Data;

    /// <summary>
    /// The EmployeeEditor window ViewModel class in the MVVM pattern.
    /// </summary>
    public class EmployeeEditorViewModel : ViewModelBase
    {
        private Employee employee;
        private bool isAdd;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeEditorViewModel"/> class.
        /// </summary>
        public EmployeeEditorViewModel()
        {
            employee = new Employee();
            if (IsInDesignMode)
            {
                employee.Name = "Sample Name";
                employee.Type = JobType.Accountant;
                employee.Wage = 100;
                employee.DateOfBirth = System.DateTime.Now;
                employee.EmployeeId = "ABCD1234";
                employee.HireDate = System.DateTime.Now;
            }
        }

        /// <summary>
        /// Gets the all job types as an array.
        /// </summary>
        public static Array JobTypes
        {
            get { return Enum.GetValues(typeof(JobType)); }
        }

        /// <summary>
        /// Gets or sets the Employee displayed in the windows.
        /// </summary>
        public Employee Employee
        {
            get { return employee; }
            set { Set(ref employee, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether gets the window is in add mode or not.
        /// </summary>
        public bool IsAdd
        {
            get { return isAdd; }
            set { Set(ref isAdd, value); }
        }
    }
}
