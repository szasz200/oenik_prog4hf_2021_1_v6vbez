﻿namespace PublicTransportWPF.VM
{
    using GalaSoft.MvvmLight;

    /// <summary>
    /// The main window ViewModel class in the MVVM pattern.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
    }
}
