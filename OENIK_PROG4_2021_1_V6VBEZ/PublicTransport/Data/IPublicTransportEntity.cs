﻿namespace PublicTransportWPF.Data
{
    using System.Collections.Generic;

    /// <summary>
    /// Represents any entity from PublicTransport app.
    /// </summary>
    public interface IPublicTransportEntity
    {
        /// <summary>
        /// Add a new entity to the logic.
        /// </summary>
        void AddMethod();

        /// <summary>
        /// Returns a list with the entities.
        /// </summary>
        /// <returns>A list with the all instances of an entity.</returns>
        IList<IPublicTransportEntity> GetAllMethod();

        /// <summary>
        /// Remove an entity from the logic.
        /// </summary>
        void DeleteMethod();

        /// <summary>
        /// Modify an existing entity.
        /// </summary>
        void ModifyMethod();

        /// <summary>
        /// Creates a copy from the "other" entity.
        /// </summary>
        /// <param name="other">An entity instance filled with datas.</param>
        public void CopyFrom(IPublicTransportEntity other);
    }
}
