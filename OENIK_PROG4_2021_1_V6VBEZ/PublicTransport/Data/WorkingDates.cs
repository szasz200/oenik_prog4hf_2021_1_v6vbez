﻿namespace PublicTransportWPF.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// This class represents a workdate of a driver.
    /// </summary>
    public partial class WorkingDate : ObservableObject, IPublicTransportEntity
    {
        private int workingId;
        private DateTime date;
        private string lineId;
        private string lpnumber;
        private string employeeId;
        private int workingMinutes;
        private int consumption;

        /// <summary>
        /// Gets or sets the WorkingId of a WorkingDate entity, which is a identifier of a working day.
        /// </summary>
        public int WorkingId
        {
            get { return workingId; }
            set { Set(ref workingId, value); }
        }

        /// <summary>
        /// Gets or sets the Date of a WorkingDate entity, which indicates the date of the working day.
        /// </summary>
        public DateTime Date
        {
            get { return date; }
            set { Set(ref date, value); }
        }

        /// <summary>
        /// Gets or sets the LineId of a WorkingDate entity, which is a lineId where the driver worked.
        /// </summary>
        public string LineId
        {
            get { return lineId; }
            set { Set(ref lineId, value); }
        }

        /// <summary>
        /// Gets or sets the Lpnumber of a WorkingDate entity, which is a lpnumber of the vehicle with which the driver worked.
        /// </summary>
        public string Lpnumber
        {
            get { return lpnumber; }
            set { Set(ref lpnumber, value); }
        }

        /// <summary>
        /// Gets or sets the EmployeeId of a WorkingDate entity, which is a EmployeeId of the worker.
        /// </summary>
        public string EmployeeId
        {
            get { return employeeId; }
            set { Set(ref employeeId, value); }
        }

        /// <summary>
        /// Gets or sets the WorkingMinutes of a WorkingDate entity, which indicates number of minutes which the employee worked that day.
        /// </summary>
        public int WorkingMinutes
        {
            get { return workingMinutes; }
            set { Set(ref workingMinutes, value); }
        }

        /// <summary>
        /// Gets or sets the Consumption of a WorkingDate entity, which indicates the amount of consumed electricity or fuel.
        /// </summary>
        public int Consumption
        {
            get { return consumption; }
            set { Set(ref consumption, value); }
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Workdate id:{WorkingId}, Date:{Date.ToShortDateString()}, Employee:{EmployeeId}, Vehicle:{Lpnumber}, Line:{LineId}, Worked minutes:{WorkingMinutes}, Consumption:{Consumption} ;";
        }

        /// <summary>
        /// Creates a copy from the "other" WorkingDate.
        /// </summary>
        /// <param name="other">An WorkingDate instance filled with datas.</param>
        public void CopyFrom(IPublicTransportEntity other)
        {
            this.GetType().GetProperties().ToList().ForEach(
                property => property.SetValue(this, property.GetValue(other as WorkingDate)));
        }

        /// <inheritdoc/>
        public void AddMethod()
        {
            PublicTransport.Data.Models.WorkingDate tmp = new PublicTransport.Data.Models.WorkingDate();
            tmp.GetType().GetProperties().ToList().ForEach(
                prop => prop.SetValue(tmp, prop.GetValue(this)));
            Factory.AccountantLogic.NewWorkDate(tmp);
        }

        /// <inheritdoc/>
        public IList<IPublicTransportEntity> GetAllMethod()
        {
            IList<IPublicTransportEntity> result = new List<IPublicTransportEntity>();
            Factory.AccountantLogic.GetWorkingDates().ToList().ForEach(
                rec => result.Add(new WorkingDate()
                {
                    WorkingId = rec.WorkingId,
                    EmployeeId = rec.EmployeeId,
                    Lpnumber = rec.Lpnumber,
                    WorkingMinutes = rec.WorkingMinutes,
                    Consumption = rec.Consumption,
                    Date = rec.Date,
                    LineId = rec.LineId,
                }));
            return result;
        }

        /// <inheritdoc/>
        public void DeleteMethod()
        {
        }

        /// <inheritdoc/>
        public void ModifyMethod()
        {
        }
    }
}
