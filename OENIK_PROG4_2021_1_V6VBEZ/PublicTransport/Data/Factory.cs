﻿namespace PublicTransportWPF.Data
{
    using System;
    using System.Linq;
    using PublicTransport.Data.Models;
    using PublicTransport.Logic;
    using PublicTransport.Repository;

    /// <summary>
    /// Creates instantiation of logics.
    /// </summary>
    internal static class Factory
    {
        private static PTDBContext pTDB = new PTDBContext();
        private static EmployeeRepository employeeRepository = new EmployeeRepository(pTDB);
        private static VehicleRepository vehicleRepository = new VehicleRepository(pTDB);
        private static LineRepository lineRepository = new LineRepository(pTDB);
        private static WorkingDateRepository workingDateRepository = new WorkingDateRepository(pTDB);
        private static MaintenanceRepository maintenanceRepository = new MaintenanceRepository(pTDB);
        private static HumanResourceLogic humanResourceLogic = new HumanResourceLogic(employeeRepository);
        private static AccountantLogic accountantLogic = new AccountantLogic(workingDateRepository, maintenanceRepository, employeeRepository);
        private static NetworkManagementLogic networkManagementLogic = new NetworkManagementLogic(lineRepository);
        private static VehicleManagementLogic vehicleManagementLogic = new VehicleManagementLogic(vehicleRepository, maintenanceRepository);

        static Factory()
        {
            if (!HumanResourceLogic.GetAllEmployee().Any())
            {
                AddData(pTDB);
            }
        }

        /// <summary>
        /// Gets the tools for the human resource department.
        /// </summary>
        internal static HumanResourceLogic HumanResourceLogic
        {
            get { return humanResourceLogic; }
        }

        /// <summary>
        /// Gets the tools for the accounting department.
        /// </summary>
        internal static AccountantLogic AccountantLogic
        {
            get { return accountantLogic; }
        }

        /// <summary>
        /// Gets the tools for the network management department.
        /// </summary>
        internal static NetworkManagementLogic NetworkManagementLogic
        {
            get { return networkManagementLogic; }
        }

        /// <summary>
        /// Gets the tools for the vehicle management department.
        /// </summary>
        internal static VehicleManagementLogic VehicleManagementLogic
        {
            get { return vehicleManagementLogic; }
        }

        private static void AddData(PTDBContext pTDB)
        {
            PublicTransport.Data.Models.Employee employee1 = new PublicTransport.Data.Models.Employee() { EmployeeId = "A365F2D1", DateOfBirth = new DateTime(1985, 06, 15), HireDate = new DateTime(2019, 02, 11), Name = "Gábor Kovács", Type = "Busdriver", Wage = 220000 };
            PublicTransport.Data.Models.Employee employee2 = new PublicTransport.Data.Models.Employee() { EmployeeId = "F0214A58", DateOfBirth = new DateTime(1995, 11, 15), HireDate = new DateTime(2018, 09, 14), Name = "Bernadett Nagy", Type = "Tramdriver", Wage = 280000 };
            PublicTransport.Data.Models.Employee employee3 = new PublicTransport.Data.Models.Employee() { EmployeeId = "01B14A38", DateOfBirth = new DateTime(1991, 10, 22), HireDate = new DateTime(2018, 06, 24), Name = "Ervin Molnár", Type = "Maintainer", Wage = 270000 };
            PublicTransport.Data.Models.Employee employee4 = new PublicTransport.Data.Models.Employee() { EmployeeId = "B0B15B99", DateOfBirth = new DateTime(1999, 07, 02), HireDate = new DateTime(2020, 07, 21), Name = "Johanna Kun", Type = "Maintainer", Wage = 250000 };
            PublicTransport.Data.Models.Employee employee5 = new PublicTransport.Data.Models.Employee() { EmployeeId = "12BFA515", DateOfBirth = new DateTime(1989, 12, 11), HireDate = new DateTime(2018, 03, 12), Name = "Patrik Földesi", Type = "Accountant", Wage = 350000 };
            PublicTransport.Data.Models.Employee employee6 = new PublicTransport.Data.Models.Employee() { EmployeeId = "F2837A5C", DateOfBirth = new DateTime(1990, 02, 10), HireDate = new DateTime(2019, 12, 15), Name = "Károly Kocsis", Type = "Busdriver", Wage = 220000 };
            PublicTransport.Data.Models.Employee employee7 = new PublicTransport.Data.Models.Employee() { EmployeeId = "12D35E89", DateOfBirth = new DateTime(1999, 09, 30), HireDate = new DateTime(2019, 11, 02), Name = "Henrietta Kovács", Type = "Trolleydriver", Wage = 320000 };
            pTDB.Add(employee1);
            pTDB.Add(employee2);
            pTDB.Add(employee3);
            pTDB.Add(employee4);
            pTDB.Add(employee5);
            pTDB.Add(employee6);
            pTDB.Add(employee7);
            pTDB.SaveChanges();
            /*PublicTransport.Data.Models.Vehicle vehicle1 = new PublicTransport.Data.Models.Vehicle() { Lpnumber = "BPO-127", Type = "Bus", YearOfManufacture = 2011, TraveledDistance = 123654.7M, LastMaintenanceId = null, NextMaintenance = new DateTime(2020, 11, 11) };
            PublicTransport.Data.Models.Vehicle vehicle2 = new PublicTransport.Data.Models.Vehicle() { Lpnumber = "2345", Type = "Tram", YearOfManufacture = 2015, TraveledDistance = 106651.2M, LastMaintenanceId = null, NextMaintenance = new DateTime(2020, 11, 19) };
            PublicTransport.Data.Models.Vehicle vehicle3 = new PublicTransport.Data.Models.Vehicle() { Lpnumber = "BPO-234", Type = "Trolley", YearOfManufacture = 2019, TraveledDistance = 14854.7M, LastMaintenanceId = null, NextMaintenance = new DateTime(2020, 12, 19) };
            PublicTransport.Data.Models.Vehicle vehicle4 = new PublicTransport.Data.Models.Vehicle() { Lpnumber = "4358", Type = "Tram", YearOfManufacture = 2016, TraveledDistance = 100624.1M, LastMaintenanceId = null, NextMaintenance = new DateTime(2020, 11, 25) };
            PublicTransport.Data.Models.Vehicle vehicle5 = new PublicTransport.Data.Models.Vehicle() { Lpnumber = "BPO-467", Type = "Bus", YearOfManufacture = 2014, TraveledDistance = 106651.2M, LastMaintenanceId = null, NextMaintenance = new DateTime(2020, 11, 29) };
            pTDB.Add(vehicle1);
            pTDB.Add(vehicle2);
            pTDB.Add(vehicle3);
            pTDB.Add(vehicle4);
            pTDB.Add(vehicle5);
            pTDB.SaveChanges();
            PublicTransport.Data.Models.Maintenance maintenance1 = new PublicTransport.Data.Models.Maintenance() { MaintenanceId = 2, Cost = 19657, Date = DateTime.Now.Date, EmployeeId = "01B14A38", Lpnumber = "BPO-127", Operations = "painting" };
            PublicTransport.Data.Models.Maintenance maintenance2 = new PublicTransport.Data.Models.Maintenance() { MaintenanceId = 3, Date = DateTime.Now.Date, EmployeeId = "B0B15B99", Lpnumber = "2345", Operations = "Seat change", Cost = 12458 };
            PublicTransport.Data.Models.Maintenance maintenance3 = new PublicTransport.Data.Models.Maintenance() { MaintenanceId = 4, Date = new DateTime(2020, 12, 01), EmployeeId = "B0B15B99", Lpnumber = "4358", Operations = "Pantograph change", Cost = 800000 };
            PublicTransport.Data.Models.Maintenance maintenance4 = new PublicTransport.Data.Models.Maintenance() { MaintenanceId = 5, Date = new DateTime(2020, 11, 01), EmployeeId = "01B14A38", Lpnumber = "BPO-467", Operations = "Break disk change", Cost = 35000 };
            pTDB.Add(maintenance1);
            pTDB.Add(maintenance2);
            pTDB.Add(maintenance3);
            pTDB.Add(maintenance4);
            pTDB.SaveChanges();
            PublicTransport.Data.Models.Line line = new PublicTransport.Data.Models.Line() { LineId = "5A", Length = 12.6M, NumberOfStops = 13, NumberOfVehicles = 5, TimeRequired = 56, Type = "Bus" };
            PublicTransport.Data.Models.Line line1 = new PublicTransport.Data.Models.Line() { LineId = "72", Length = 6.6M, NumberOfStops = 18, NumberOfVehicles = 5, TimeRequired = 48, Type = "Trolley" };
            PublicTransport.Data.Models.Line line2 = new PublicTransport.Data.Models.Line() { LineId = "17", Length = 9.6M, NumberOfStops = 17, NumberOfVehicles = 8, TimeRequired = 61, Type = "Tram" };
            PublicTransport.Data.Models.Line line3 = new PublicTransport.Data.Models.Line() { LineId = "56", Length = 13.4M, NumberOfStops = 21, NumberOfVehicles = 6, TimeRequired = 61, Type = "Tram" };
            pTDB.Add(line);
            pTDB.Add(line1);
            pTDB.Add(line2);
            pTDB.Add(line3);
            pTDB.SaveChanges();
            PublicTransport.Data.Models.WorkingDate workingDate = new PublicTransport.Data.Models.WorkingDate() { EmployeeId = "A365F2D1", Date = DateTime.Now.Date, LineId = "5A", Consumption = 23, Lpnumber = "BPO-127", WorkingId = 1, WorkingMinutes = 499 };
            PublicTransport.Data.Models.WorkingDate workingDate1 = new PublicTransport.Data.Models.WorkingDate() { EmployeeId = "F0214A58", Date = DateTime.Now.Date, LineId = "56", Consumption = 3500, Lpnumber = "2345", WorkingId = 2, WorkingMinutes = 518 };
            PublicTransport.Data.Models.WorkingDate workingDate2 = new PublicTransport.Data.Models.WorkingDate() { EmployeeId = "F0214A58", Date = new DateTime(2020, 12, 01), LineId = "17", Consumption = 1350, Lpnumber = "2345", WorkingId = 3, WorkingMinutes = 597 };
            PublicTransport.Data.Models.WorkingDate workingDate3 = new PublicTransport.Data.Models.WorkingDate() { EmployeeId = "F2837A5C", Date = new DateTime(2020, 12, 01), LineId = "5A", Consumption = 23, Lpnumber = "BPO-127", WorkingId = 4, WorkingMinutes = 548 };
            PublicTransport.Data.Models.WorkingDate workingDate4 = new PublicTransport.Data.Models.WorkingDate() { EmployeeId = "12D35E89", Date = new DateTime(2020, 11, 30), LineId = "72", Consumption = 230, Lpnumber = "BPO-234", WorkingId = 5, WorkingMinutes = 598 };
            pTDB.Add(workingDate);
            pTDB.Add(workingDate1);
            pTDB.Add(workingDate2);
            pTDB.Add(workingDate3);
            pTDB.Add(workingDate4);
            pTDB.SaveChanges();*/
        }
    }
}
