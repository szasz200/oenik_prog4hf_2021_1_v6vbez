﻿namespace PublicTransportWPF.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// This class represents a Vehicle.
    /// </summary>
    public partial class Vehicle : ObservableObject, IPublicTransportEntity
    {
        private string lpnumber;
        private VType type;
        private int yearOfManufacture;
        private double traveledDistance;
        private int? lastMaintenanceId;
        private DateTime? nextMaintenance;

        /// <summary>
        /// Gets or sets the Lpnumber of a Vehicle entity, which is the license plate number of the vehicle as the identifier of a vehicle.
        /// </summary>
        public string Lpnumber
        {
            get { return lpnumber; }
            set { Set(ref lpnumber, value); }
        }

        /// <summary>
        /// Gets or sets the Type of a Vehicle entity, which indicates the type(bus, troley or tram) of the vehicle.
        /// </summary>
        public VType Type
        {
            get { return type; }
            set { Set(ref type, value); }
        }

        /// <summary>
        /// Gets or sets the YearOfManufacture of a Vehicle entity, which indicates the the year of manufacture of a vehicle.
        /// </summary>
        public int YearOfManufacture
        {
            get { return yearOfManufacture; }
            set { Set(ref yearOfManufacture, value); }
        }

        /// <summary>
        /// Gets or sets the TraveledDistance of a Vehicle entity, which indicates the position of the odometer.
        /// </summary>
        public double TraveledDistance
        {
            get { return traveledDistance; }
            set { Set(ref traveledDistance, value); }
        }

        /// <summary>
        /// Gets or sets the LastMaintenanceId of a Vehicle entity, which indicates the last maintenance. If the vehicle is new, the LastMaintenanceId is null.
        /// </summary>
        public int? LastMaintenanceId
        {
            get { return lastMaintenanceId; }
            set { Set(ref lastMaintenanceId, value); }
        }

        /// <summary>
        /// Gets or sets the NextMaintenance of a Vehicle entity, which indicates the date, when the vehicle needs to be inspected.
        /// </summary>
        public DateTime? NextMaintenance
        {
            get { return nextMaintenance; }
            set { Set(ref nextMaintenance, value); }
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"License plate number:{Lpnumber}, Type:{Type}, Year of manufacture:{YearOfManufacture}, Traveled distance:{TraveledDistance}km, LastMaintenanceId:{LastMaintenanceId}, Next maintenance date:{NextMaintenance.GetValueOrDefault().ToShortDateString()} ;";
        }

        /// <summary>
        /// Creates a copy from the "other" Vehicle.
        /// </summary>
        /// <param name="other">An Vehicle instance filled with datas.</param>
        public void CopyFrom(IPublicTransportEntity other)
        {
            this.GetType().GetProperties().ToList().ForEach(
                property => property.SetValue(this, property.GetValue(other as Vehicle)));
        }

        /// <inheritdoc/>
        public void AddMethod()
        {
            PublicTransport.Data.Models.Vehicle tmp = new PublicTransport.Data.Models.Vehicle();
            tmp.GetType().GetProperties().ToList().ForEach(
                prop => prop.SetValue(tmp, prop.GetValue(this)));
            Factory.VehicleManagementLogic.NewVehicle(tmp);
        }

        /// <inheritdoc/>
        public IList<IPublicTransportEntity> GetAllMethod()
        {
            IList<IPublicTransportEntity> result = new List<IPublicTransportEntity>();
            Factory.VehicleManagementLogic.Vehicles().ToList().ForEach(
                rec => result.Add(new Vehicle()
                {
                    Lpnumber = rec.Lpnumber,
                    Type = (VType)Enum.Parse(typeof(VType), rec.Type, true),
                    TraveledDistance = (double)rec.TraveledDistance,
                    YearOfManufacture = rec.YearOfManufacture,
                    LastMaintenanceId = rec.LastMaintenanceId,
                    NextMaintenance = rec.NextMaintenance,
                }));
            return result;
        }

        /// <inheritdoc/>
        public void DeleteMethod()
        {
            Factory.VehicleManagementLogic.RemoveVehicle(this.Lpnumber);
        }

        /// <inheritdoc/>
        public void ModifyMethod()
        {
            Factory.VehicleManagementLogic.ChangeNextMaintenance(this.Lpnumber, this.NextMaintenance.GetValueOrDefault());
        }
    }
}
