﻿namespace PublicTransportWPF.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// This class represents an Employee in the UI.
    /// </summary>
    public class Employee : ObservableObject, IPublicTransportEntity
    {
        private string employeeId;
        private JobType type;
        private string name;
        private DateTime dateOfBirth;
        private DateTime hireDate;
        private int wage;

        /// <summary>
        /// Gets or sets the EmployeeID of an Employee entity, which is a identifier of an employee.
        /// </summary>
        public string EmployeeId
        {
            get { return employeeId; }
            set { Set(ref employeeId, value); }
        }

        /// <summary>
        /// Gets or sets the Type of an Employee entity, which indicates the job type of the employee.
        /// </summary>
        public JobType Type
        {
            get { return type; }
            set { Set(ref type, value); }
        }

        /// <summary>
        /// Gets or sets the Name of an Employee entity.
        /// </summary>
        public string Name
        {
            get { return name; }
            set { Set(ref name, value); }
        }

        /// <summary>
        /// Gets or sets the Date of birth of an Employee entity.
        /// </summary>
        public DateTime DateOfBirth
        {
            get { return dateOfBirth; }
            set { Set(ref dateOfBirth, value); }
        }

        /// <summary>
        /// Gets or sets the HireDate of an Employee entity, which indicates the date when the employee started working for the company.
        /// </summary>
        public DateTime HireDate
        {
            get { return hireDate; }
            set { Set(ref hireDate, value); }
        }

        /// <summary>
        /// Gets or sets the wage of an Employee entity, which indicates the salary of the employee.
        /// </summary>
        public int Wage
        {
            get { return wage; }
            set { Set(ref wage, value); }
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Employee Id:{EmployeeId}, Name:{Name}, Date of birth: {DateOfBirth.ToShortDateString()}, Type:{Type}, Hire date:{HireDate.ToShortDateString()}, Wage:{Wage} ;";
        }

        /// <summary>
        /// Creates a copy from the "other" Employee.
        /// </summary>
        /// <param name="other">An Employee instance filled with datas.</param>
        public void CopyFrom(IPublicTransportEntity other)
        {
            Employee tmp = other as Employee;
            this.GetType().GetProperties().ToList().ForEach(
                property => property.SetValue(this, property.GetValue(tmp)));
        }

        /// <inheritdoc/>
        public void AddMethod()
        {
            if (this.EmployeeId is not null)
            {
                PublicTransport.Data.Models.Employee tmp = new PublicTransport.Data.Models.Employee();
                tmp.Name = this.Name;
                tmp.EmployeeId = this.EmployeeId.ToUpperInvariant();
                tmp.DateOfBirth = this.DateOfBirth;
                tmp.HireDate = this.HireDate;
                tmp.Type = this.Type.ToString();
                tmp.Wage = this.Wage;
                Factory.HumanResourceLogic.NewEmployee(tmp);
            }
            else
            {
                throw new InvalidOperationException("The EmployeeId is null.");
            }
        }

        /// <inheritdoc/>
        public IList<IPublicTransportEntity> GetAllMethod()
        {
            IList<IPublicTransportEntity> result = new List<IPublicTransportEntity>();
            Factory.HumanResourceLogic.GetAllEmployee().ToList().ForEach(
                rec => result.Add(new Employee()
                {
                    EmployeeId = rec.EmployeeId,
                    Type = (JobType)Enum.Parse(typeof(JobType), rec.Type, true),
                    Name = rec.Name,
                    DateOfBirth = rec.DateOfBirth,
                    HireDate = rec.HireDate,
                    Wage = rec.Wage,
                }));
            return result;
        }

        /// <inheritdoc/>
        public void DeleteMethod()
        {
            Factory.HumanResourceLogic.RemoveEmployee(this.employeeId);
        }

        /// <inheritdoc/>
        public void ModifyMethod()
        {
                Factory.HumanResourceLogic.ChangeName(this.EmployeeId, this.Name);
                Factory.HumanResourceLogic.ChangePositon(this.EmployeeId, this.Type.ToString());
                Factory.HumanResourceLogic.ChangeWage(this.EmployeeId, this.Wage);
        }
    }
}
