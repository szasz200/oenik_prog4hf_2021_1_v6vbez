﻿namespace PublicTransportWPF.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// This class represents a maintenance operation.
    /// </summary>
    public partial class Maintenance : ObservableObject, IPublicTransportEntity
    {
        private int maintenanceId;
        private DateTime date;
        private string lpnumber;
        private string employeeId;
        private int? cost;
        private string operations;

        /// <summary>
        /// Gets or sets the MaintenanceID of a Maintenance entity, which is a identifier of a maintenance.
        /// </summary>
        public int MaintenanceId
        {
            get { return maintenanceId; }
            set { Set(ref maintenanceId, value); }
        }

        /// <summary>
        /// Gets or sets the Date of a Maintenance entity, which indicates the date when the maintenance was performed.
        /// </summary>
        public DateTime Date
        {
            get { return date; }
            set { Set(ref date, value); }
        }

        /// <summary>
        /// Gets or sets the Lpnumber of a Maintenance entity, which indicates the vehicle which was maintained.
        /// </summary>
        public string Lpnumber
        {
            get { return lpnumber; }
            set { Set(ref lpnumber, value); }
        }

        /// <summary>
        /// Gets or sets the EmployeeId of a Maintenance entity, which indicates the employee who performed the maintenance.
        /// </summary>
        public string EmployeeId
        {
            get { return employeeId; }
            set { Set(ref employeeId, value); }
        }

        /// <summary>
        /// Gets or sets the Cost of a Maintenance entity, which indicates the cost of the maintenance.
        /// </summary>
        public int? Cost
        {
            get { return cost; }
            set { Set(ref cost, value); }
        }

        /// <summary>
        /// Gets or sets the Operations of a Maintenance entity, which describes the operations performed during maintenance.
        /// </summary>
        public string Operations
        {
            get { return operations; }
            set { Set(ref operations, value); }
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Maintenance id:{MaintenanceId}, Date:{Date.ToShortDateString()}, License plate number:{Lpnumber}, Employee id:{EmployeeId}, Cost:{Cost}, Operations:{Operations} ;";
        }

        /// <summary>
        /// Creates a copy from the "other" Maintenance.
        /// </summary>
        /// <param name="other">An Maintenance instance filled with datas.</param>
        public void CopyFrom(IPublicTransportEntity other)
        {
            Maintenance tmp = other as Maintenance;
            this.GetType().GetProperties().ToList().ForEach(
                property => property.SetValue(this, property.GetValue(tmp)));
        }

        /// <inheritdoc/>
        public void AddMethod()
        {
            PublicTransport.Data.Models.Maintenance tmp = new PublicTransport.Data.Models.Maintenance();
            tmp.GetType().GetProperties().ToList().ForEach(
                prop => prop.SetValue(tmp, prop.GetValue(this)));
            Factory.VehicleManagementLogic.NewMaintenance(tmp);
        }

        /// <inheritdoc/>
        public IList<IPublicTransportEntity> GetAllMethod()
        {
            IList<IPublicTransportEntity> result = new List<IPublicTransportEntity>();
            Factory.AccountantLogic.GetMaintenances().ToList().ForEach(
                rec => result.Add(new Maintenance()
                {
                    MaintenanceId = rec.MaintenanceId,
                    EmployeeId = rec.EmployeeId,
                    Cost = rec.Cost,
                    Date = rec.Date,
                    Operations = rec.Operations,
                    Lpnumber = rec.Lpnumber,
                }));
            return result;
        }

        /// <inheritdoc/>
        public void DeleteMethod()
        {
        }

        /// <inheritdoc/>
        public void ModifyMethod()
        {
        }
    }
}
