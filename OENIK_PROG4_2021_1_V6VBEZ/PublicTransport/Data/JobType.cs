﻿namespace PublicTransportWPF.Data
{
    /// <summary>
    /// List of jobs at the company.
    /// </summary>
    public enum JobType
    {
        /// <summary>
        /// Person who drives buses.
        /// </summary>
        BusDriver,

        /// <summary>
        /// Person who drives trams.
        /// </summary>
        TramDriver,

        /// <summary>
        /// Person who repairs the tools and the vehicles.
        /// </summary>
        Maintainer,

        /// <summary>
        /// Accountant.
        /// </summary>
        Accountant,

        /// <summary>
        /// Person who drives trolleybuses.
        /// </summary>
        TrolleyDriver,

        /// <summary>
        /// Person who mananges a part of the company.
        /// </summary>
        Mananger,

        /// <summary>
        /// Person who handles matters.
        /// </summary>
        Administrator,
    }
}
