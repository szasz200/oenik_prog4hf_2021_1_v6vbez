﻿namespace PublicTransportWPF.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// This class represents a line in the city.
    /// </summary>
    public partial class Line : ObservableObject, IPublicTransportEntity
    {
        private string lineId;
        private VType type;
        private double length;
        private int timeRequired;
        private int numberOfStops;
        private int numberOfVehicles;

        /// <summary>
        /// Gets or sets the LineID of a Line entity, which is a identifier of a line.
        /// </summary>
        public string LineId
        {
            get { return lineId; }
            set { Set(ref lineId, value); }
        }

        /// <summary>
        /// Gets or sets the Type of a Line entity, which indicates the type(bus, troley or tram) of the line.
        /// </summary>
        public VType Type
        {
            get { return type; }
            set { Set(ref type, value); }
        }

        /// <summary>
        /// Gets or sets the Length of a Line entity, which indicates the length of the line in kilometers.
        /// </summary>
        public double Length
        {
            get { return length; }
            set { Set(ref length, value); }
        }

        /// <summary>
        /// Gets or sets the TimeRequired of a Line entity, which indicates the minutes, which is required to traverse the line.
        /// </summary>
        public int TimeRequired
        {
            get { return timeRequired; }
            set { Set(ref timeRequired, value); }
        }

        /// <summary>
        /// Gets or sets the NumberOfStop of a Line entity, which indicates the number of stop along the line.
        /// </summary>
        public int NumberOfStops
        {
            get { return numberOfStops; }
            set { Set(ref numberOfStops, value); }
        }

        /// <summary>
        /// Gets or sets the NumberOfVehicles of a Line entity, which indicates the number of vechicles on the line at the same time.
        /// </summary>
        public int NumberOfVehicles
        {
            get { return numberOfVehicles; }
            set { Set(ref numberOfVehicles, value); }
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Line id:{LineId}, Line type:{Type}, Length:{Length}km, Required time:{TimeRequired}, Number of stops:{NumberOfStops}, Number of vehicles:{NumberOfVehicles} ;";
        }

        /// <summary>
        /// Creates a copy from the "other" Line.
        /// </summary>
        /// <param name="other">An Line instance filled with datas.</param>
        public void CopyFrom(IPublicTransportEntity other)
        {
            this.GetType().GetProperties().ToList().ForEach(
                property => property.SetValue(this, property.GetValue(other as Line)));
        }

        /// <inheritdoc/>
        public void AddMethod()
        {
            PublicTransport.Data.Models.Line tmp = new PublicTransport.Data.Models.Line();
            tmp.GetType().GetProperties().ToList().ForEach(
                prop => prop.SetValue(tmp, prop.GetValue(this)));
            Factory.NetworkManagementLogic.NewLine(tmp);
        }

        /// <inheritdoc/>
        public IList<IPublicTransportEntity> GetAllMethod()
        {
            IList<IPublicTransportEntity> result = new List<IPublicTransportEntity>();
            Factory.NetworkManagementLogic.GetLines().ToList().ForEach(
                rec => result.Add(new Line()
                {
                    LineId = rec.LineId,
                    Type = (VType)Enum.Parse(typeof(VType), rec.Type, true),
                    Length = (double)rec.Length,
                    NumberOfStops = rec.NumberOfStops,
                    NumberOfVehicles = rec.NumberOfVehicles,
                    TimeRequired = rec.TimeRequired,
                }));
            return result;
        }

        /// <inheritdoc/>
        public void DeleteMethod()
        {
            Factory.NetworkManagementLogic.ChangeNumberOfStops(LineId, 0);
        }

        /// <inheritdoc/>
        public void ModifyMethod()
        {
            Factory.NetworkManagementLogic.ChangeLength(this.LineId, (decimal)this.Length);
            Factory.NetworkManagementLogic.ChangeNumberOfStops(this.LineId, this.NumberOfStops);
            Factory.NetworkManagementLogic.ChangeNumberOfVehicles(this.LineId, this.NumberOfVehicles);
            Factory.NetworkManagementLogic.ChangeTimeRequired(this.LineId, this.TimeRequired);
        }
    }
}
