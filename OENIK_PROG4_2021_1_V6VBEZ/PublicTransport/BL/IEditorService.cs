﻿namespace PublicTransportWPF.BL
{
    using PublicTransportWPF.Data;

    /// <summary>
    /// A service for editing an entity without dependecing.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// Edit an entity.
        /// </summary>
        /// <param name="entity">An entity instance for editing.</param>
        /// <param name="newEntity">True if this is an add method.</param>
        /// <returns>True if the editing was ok, false if the editing was canceled.</returns>
        bool EditEntity(IPublicTransportEntity entity, bool newEntity);
    }
}
