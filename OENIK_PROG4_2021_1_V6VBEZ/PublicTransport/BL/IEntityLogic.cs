﻿namespace PublicTransportWPF.BL
{
    using System.Collections.Generic;
    using PublicTransportWPF.Data;

    /// <summary>
    /// Logic for the CRUD methods of an entity.
    /// </summary>
    public interface IEntityLogic
    {
        /// <summary>
        /// Add a new entity to the list.
        /// </summary>
        /// <param name="list">The list of entities. </param>
        void AddEntity(IList<IPublicTransportEntity> list);

        /// <summary>
        /// Modify an existing entity.
        /// </summary>
        /// <param name="entityToModify"> The entity to modify. </param>
        void ModifyEntity(IPublicTransportEntity entityToModify);

        /// <summary>
        /// Remove an entity from the list.
        /// </summary>
        /// <param name="list">The list of entities. </param>
        /// <param name="entity"> The entity to remove. </param>
        void DeleteEntity(IList<IPublicTransportEntity> list, IPublicTransportEntity entity);

        /// <summary>
        /// Returns a list with the entities.
        /// </summary>
        /// <param name="entity"> An IPublicTransportEntity entity. </param>
        /// <returns>A list with the all entities.</returns>
        IList<IPublicTransportEntity> GetAllEntity(IPublicTransportEntity entity);
    }
}