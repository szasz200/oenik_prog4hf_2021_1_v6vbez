﻿namespace PublicTransportWPF.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GalaSoft.MvvmLight.Messaging;
    using PublicTransportWPF.Data;

    /// <summary>
    /// The implemetation of IEntityLogic interface (implemts CRUD methods).
    /// </summary>
    /// <typeparam name="T"> An entity from the Data Layer.</typeparam>
    public class EntityLogic<T> : IEntityLogic
        where T : IPublicTransportEntity
    {
        private IEditorService editorService;
        private IMessenger messengerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityLogic{T}"/> class.
        /// </summary>
        /// <param name="editorService">FIX ME.</param>
        /// <param name="messengerService">FIX ME2.</param>
        public EntityLogic(IEditorService editorService, IMessenger messengerService)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
        }

        /// <inheritdoc/>
        public void AddEntity(IList<IPublicTransportEntity> list)
        {
            IPublicTransportEntity newEntity = (T)Activator.CreateInstance(typeof(T));
            if (editorService.EditEntity(newEntity, true))
            {
                // list?.Add(newEntity);
                try
                {
                    newEntity.AddMethod();
                    list.ToList().ForEach(x => list.Remove(x));
                    GetAllEntity(newEntity).ToList().ForEach(x => list.Add(x));
                    messengerService.Send("ADD OK", "LogicResult");
                }
                catch (InvalidOperationException e)
                {
                    list?.Remove(newEntity);
                    messengerService.Send("ADD FAILED \n" + e.Message, "LogicResult");
                }
            }
            else
            {
                messengerService.Send("ADD CANCEL", "LogicResult");
            }
        }

        /// <inheritdoc/>
        public void DeleteEntity(IList<IPublicTransportEntity> list, IPublicTransportEntity entity)
        {
            if (entity != null && list != null && list.Remove(entity))
            {
                entity.DeleteMethod();
                messengerService.Send("DELETE OK", "LogicResult");
            }
            else
            {
                messengerService.Send("DELETE FAILED", "LogicResult");
            }
        }

        /// <inheritdoc/>
        public IList<IPublicTransportEntity> GetAllEntity(IPublicTransportEntity entity)
        {
            return entity?.GetAllMethod();
        }

        /// <inheritdoc/>
        public void ModifyEntity(IPublicTransportEntity entityToModify)
        {
            if (entityToModify is not null)
            {
                IPublicTransportEntity clone = (T)Activator.CreateInstance(typeof(T));
                clone.CopyFrom(entityToModify);
                if (editorService.EditEntity(clone, false))
                {
                    entityToModify.CopyFrom(clone);
                    entityToModify.ModifyMethod();
                    messengerService.Send("MODIFY OK", "LogicResult");
                }
                else
                {
                    messengerService.Send("MODIFY CANCEL", "LogicResult");
                }
            }
            else
            {
                messengerService.Send("EDIT FAILED", "LogicResult");
            }
        }
    }
}
