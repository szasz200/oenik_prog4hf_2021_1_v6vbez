﻿namespace PublicTransport.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Defines the required methods for the repositories.
    /// </summary>
    /// <typeparam name="T">This is an entity class.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Finds a record in the table based on a key.
        /// </summary>
        /// <param name="key">The 'key' is a key object, which identifies the record.(Typically string or int.)</param>
        /// <returns>Returns the specified record from the table, as an entity or null if not found.</returns>
        T GetOne(object key);

        /// <summary>
        /// Returns the all records from the table, as an IQueryable collection.
        /// </summary>
        /// <returns>Returns an IQueryable collection with the entities.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Inserts a new record to the table.
        /// </summary>
        /// <param name="entity"> An entity that corresponds to the table. </param>
        /// <returns>Returns true if the insertion was successful or returns false if the insertion was failed.</returns>
        bool NewRecord(T entity);

        /// <summary>
        /// Delete a record form the table based on a key.
        /// </summary>
        /// <param name="key">The 'key' is a key object, which identifies the record.(Typically string or int.)</param>
        /// <returns>Returns true if the deletion was successful or returns false if the deletion was failed.</returns>
        bool DeleteOne(object key);
    }
}
