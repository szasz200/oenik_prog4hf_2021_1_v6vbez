﻿namespace PublicTransport.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using PublicTransport.Data.Models;

    /// <summary>
    /// A vehicle repositorie class. Implements the CRUD methods for a vehicle table.
    /// </summary>
    public class VehicleRepository : Repositories<Vehicle>, IVehicleRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VehicleRepository"/> class.
        /// </summary>
        /// <param name="ctx">The DbContext class, which accesses the database.</param>
        public VehicleRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public void UpdateLastMaintenanceId(string key, int newMaintenanceId)
        {
            var vehicle = GetOne(key);
            if (vehicle == null)
            {
                throw new InvalidOperationException("not found");
            }

            vehicle.LastMaintenanceId = newMaintenanceId;
            Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateNextMaintenance(string key, DateTime newNextMaintenance)
        {
            var vehicle = GetOne(key);
            if (vehicle == null)
            {
                throw new InvalidOperationException("not found");
            }

            vehicle.NextMaintenance = newNextMaintenance;
            Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateTraveledDistance(string key, decimal newTravelDistance)
        {
            var vehicle = GetOne(key);
            if (vehicle == null)
            {
                throw new InvalidOperationException("not found");
            }

            vehicle.TraveledDistance = newTravelDistance;
            Ctx.SaveChanges();
        }
    }
}
