﻿namespace PublicTransport.Repository
{
    using System.Linq;
    using System.Runtime.Serialization;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// An abstract repository class.
    /// </summary>
    /// <typeparam name="T">This is an entity class.</typeparam>
    public abstract class Repositories<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Repositories{T}"/> class.
        /// </summary>
        /// <param name="ctx">The DbContext class, which accesses the database.</param>
        protected Repositories(DbContext ctx)
        {
            this.Ctx = ctx;
        }

        /// <summary>
        /// Gets or sets the DbContext class, which accesses the database.
        /// </summary>
        protected DbContext Ctx { get; set; }

        /// <inheritdoc/>
        public bool DeleteOne(object key)
        {
            if (this.Ctx.Set<T>().Remove(GetOne(key)) != null)
            {
                this.Ctx.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public IQueryable<T> GetAll()
        {
            return Ctx.Set<T>();
        }

        /// <inheritdoc/>
        public T GetOne(object key)
        {
            return Ctx.Set<T>().Find(key);
        }

        /// <inheritdoc/>
        public bool NewRecord(T entity)
        {
            if (entity != null)
            {
                Ctx.Add<T>(entity);
                Ctx.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
