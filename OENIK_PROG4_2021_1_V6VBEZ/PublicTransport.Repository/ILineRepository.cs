﻿namespace PublicTransport.Repository
{
    using PublicTransport.Data.Models;

    /// <summary>
    /// Defines the required methods for a LineRepository.
    /// </summary>
    public interface ILineRepository : IRepository<Line>
    {
        /// <summary>
        /// Changes the type of the line.
        /// </summary>
        /// <param name="key">The 'key' is a key object, which identifies the Line.(The 'LineId' property.)</param>
        /// <param name="type">A VType enum to which will be replaced the value of the type field.</param>
        void UpdateType(string key, VType type);

        /// <summary>
        /// Changes the Length of the line.
        /// </summary>
        /// <param name="key">The 'key' is a key object, which identifies the Line.(The 'LineId' property.)</param>
        /// <param name="newlength">A decimal number with only one decimal place, to which will be replaced the value of the Length field.</param>
        void UpdateLength(string key, decimal newlength);

        /// <summary>
        /// Changes the TimeRequired of the line.
        /// </summary>
        /// <param name="key">The 'key' is a key object, which identifies the Line.(The 'LineId' property.)</param>
        /// <param name="newTimeRequired">A non-negative intiger up to 999, to which will be replaced the value of the TimeRequired field.</param>
        void UpdateTimeRequired(string key, int newTimeRequired);

        /// <summary>
        /// Changes the NumberOfStops of the line.
        /// </summary>
        /// <param name="key">The 'key' is a key object, which identifies the Line.(The 'LineId' property.)</param>
        /// <param name="newNumberOfStops">A non-negative intiger up to 999, to which will be replaced the value of the NumberOfStops field.</param>
        void UpdateNumberOfStops(string key, int newNumberOfStops);

        /// <summary>
        /// Changes the NumberOfVehicles of the line.
        /// </summary>
        /// <param name="key">The 'key' is a key object, which identifies the Line.(The 'LineId' property.)</param>
        /// <param name="newNumberOfVehicles">A non-negative intiger up to 999, to which will be replaced the value of the NumberOfVehicles field.</param>
        void UpdateNumberOfVehicles(string key, int newNumberOfVehicles);
    }
}
