﻿namespace PublicTransport.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PublicTransport.Data.Models;

    /// <summary>
    /// Defines the required methods for a MaintananceRepository.
    /// </summary>
    public interface IMaintenanceRepository : IRepository<Maintenance>
    {
        /// <summary>
        /// Changes the Cost of the maintenance.
        /// </summary>
        /// <param name="key">The 'key' is a key object, which identifies the record.(The 'MaintenanceId' property.)</param>
        /// <param name="newCost">A non-negative intiger to which will be replaced the value of the cost field.</param>
        void UpdateCost(int key, int newCost);
    }
}
