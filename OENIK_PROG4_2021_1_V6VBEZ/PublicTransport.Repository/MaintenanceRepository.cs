﻿namespace PublicTransport.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using PublicTransport.Data.Models;

    /// <summary>
    /// A maintenance repositorie class. Implements the CRUD methods for a line table.
    /// </summary>
    public class MaintenanceRepository : Repositories<Maintenance>, IMaintenanceRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MaintenanceRepository"/> class.
        /// </summary>
        /// <param name="ctx">The DbContext class, which accesses the database.</param>
        public MaintenanceRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public void UpdateCost(int key, int newCost)
        {
            var maintenance = GetOne(key);
            if (maintenance == null)
            {
                throw new InvalidOperationException("not found");
            }

            maintenance.Cost = newCost;
            Ctx.SaveChanges();
        }
    }
}
