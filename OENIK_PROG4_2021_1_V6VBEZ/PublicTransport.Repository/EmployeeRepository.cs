﻿namespace PublicTransport.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using PublicTransport.Data.Models;

    /// <summary>
    /// An employee repositorie class. Implements the CRUD methods for an employee table.
    /// </summary>
    public class EmployeeRepository : Repositories<Employee>, IEmployeeRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeRepository"/> class.
        /// </summary>
        /// <param name="ctx">The DbContext class, which accesses the database.</param>
        public EmployeeRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public void UpdateName(string key, string newname)
        {
            var employee = GetOne(key);
            if (employee == null)
            {
                throw new InvalidOperationException("not found");
            }

            employee.Name = newname;
            Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateType(string key, string newtype)
        {
            var employee = GetOne(key);
            if (employee == null)
            {
                throw new InvalidOperationException("not found");
            }

            employee.Type = newtype;
            Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateWage(string key, int newwage)
        {
            var employee = GetOne(key);
            if (employee == null)
            {
                throw new InvalidOperationException("not found");
            }

            employee.Wage = newwage;
            Ctx.SaveChanges();
        }
    }
}
