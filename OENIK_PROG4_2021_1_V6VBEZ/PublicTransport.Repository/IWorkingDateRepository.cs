﻿namespace PublicTransport.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PublicTransport.Data.Models;

    /// <summary>
    /// Defines the required methods for a WorkingDateRepository.
    /// </summary>
    public interface IWorkingDateRepository : IRepository<WorkingDate>
    {
    }
}
