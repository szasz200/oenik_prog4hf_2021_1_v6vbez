﻿namespace PublicTransport.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PublicTransport.Data.Models;

    /// <summary>
    /// Defines the required methods for a VehicleRepository.
    /// </summary>
    public interface IVehicleRepository : IRepository<Vehicle>
    {
        /// <summary>
        /// Changes the TraveledDistance to a new value.
        /// </summary>
        /// <param name="key">The 'key' is the license plate number of the vehicle, which identifies it.</param>
        /// <param name="newTravelDistance">A decimal number with only one decimal place, to which will be replaced the value of the TravelDistance.</param>
        void UpdateTraveledDistance(string key, decimal newTravelDistance);

        /// <summary>
        /// Changes the LastMaintenanceId to a new value.
        /// </summary>
        /// <param name="key">The 'key' is the license plate number of the vehicle, which identifies it.</param>
        /// <param name="newMaintenanceId">A non-negative intiger up to 11 digits, to which will be replaced the value of the LastMaintenanceId.</param>
        void UpdateLastMaintenanceId(string key, int newMaintenanceId);

        /// <summary>
        /// Changes the NextMaintenance to a new date.
        /// </summary>
        /// <param name="key">The 'key' is the license plate number of the vehicle, which identifies it.</param>
        /// <param name="newNextMaintenance">A DateTime type to which will be replaced the value of the NextMaintenance.</param>
        void UpdateNextMaintenance(string key, DateTime newNextMaintenance);
    }
}