﻿namespace PublicTransport.Repository
{
    /// <summary>
    /// Type list for vehicles.
    /// </summary>
    public enum VType
    {
        /// <summary>
        /// The vehicle is a bus.
        /// </summary>
        Bus,

        /// <summary>
        /// The vehicle is a trolley bus.
        /// </summary>
        Trolley,

        /// <summary>
        /// The vehicle is a tram.
        /// </summary>
        Tram,
    }
}
