﻿namespace PublicTransport.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using PublicTransport.Data.Models;

    /// <summary>
    /// A line repositorie class. Implements the CRUD methods for a line table.
    /// </summary>
    public class LineRepository : Repositories<Line>, ILineRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LineRepository"/> class.
        /// </summary>
        /// <param name="ctx">The DbContext class, which accesses the database.</param>
        public LineRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public void UpdateLength(string key, decimal newlength)
        {
            var line = GetOne(key);
            if (line == null)
            {
                throw new InvalidOperationException("not found");
            }

            line.Length = newlength;
            Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateNumberOfStops(string key, int newNumberOfStops)
        {
            var line = GetOne(key);
            if (line == null)
            {
                throw new InvalidOperationException("not found");
            }

            line.NumberOfStops = newNumberOfStops;
            Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateNumberOfVehicles(string key, int newNumberOfVehicles)
        {
            var line = GetOne(key);
            if (line == null)
            {
                throw new InvalidOperationException("not found");
            }

            line.NumberOfVehicles = newNumberOfVehicles;
            Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateTimeRequired(string key, int newTimeRequired)
        {
            var line = GetOne(key);
            if (line == null)
            {
                throw new InvalidOperationException("not found");
            }

            line.TimeRequired = newTimeRequired;
            Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateType(string key, VType type)
        {
            var line = GetOne(key);
            if (line == null)
            {
                throw new InvalidOperationException("not found");
            }

            line.Type = type.ToString();
            Ctx.SaveChanges();
        }
    }
}
