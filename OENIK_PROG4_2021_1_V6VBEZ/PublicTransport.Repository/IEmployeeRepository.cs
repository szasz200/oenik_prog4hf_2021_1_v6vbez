﻿namespace PublicTransport.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using PublicTransport.Data.Models;

    /// <summary>
    /// Defines the required methods for a EmployeeRepository.
    /// </summary>
    public interface IEmployeeRepository : IRepository<Employee>
    {
        /// <summary>
        /// Changes the type of the employee.
        /// </summary>
        /// <param name="key">The 'key' is a key object, which identifies an employee.(The EmployeeId property.)</param>
        /// <param name="newtype">A string to which will be replaced the value of the type field.</param>
        void UpdateType(string key, string newtype);

        /// <summary>
        /// Changes the name of the employee.
        /// </summary>
        /// <param name="key">The 'key' is a key object, which identifies an employee.(The EmployeeId property.)</param>
        /// <param name="newname">A string to which will be replaced the value of the name field.</param>
        void UpdateName(string key, string newname);

        /// <summary>
        /// Changes the wage of the employee.
        /// </summary>
        /// <param name="key">The 'key' is a key object, which identifies an employee.(The EmployeeId property.)</param>
        /// <param name="newwage">A non-negative intiger up to 9999999 to which will be replaced the value of the wage field.</param>
        void UpdateWage(string key, int newwage);
    }
}
