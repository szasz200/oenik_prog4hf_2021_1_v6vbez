﻿namespace PublicTransport.Repository
{
    using System;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using PublicTransport.Data.Models;

    /// <summary>
    /// A WorkingDate repositorie class. Implements the CRUD methods for a WorkingDates table.
    /// </summary>
    public class WorkingDateRepository : Repositories<WorkingDate>, IWorkingDateRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkingDateRepository"/> class.
        /// </summary>
        /// <param name="ctx">The DbContext class, which accesses the database.</param>
        public WorkingDateRepository(DbContext ctx)
            : base(ctx)
        {
        }
    }
}
