﻿namespace PublicTransport.WpfClient
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// This inteface defines methods for the logic of client.
    /// </summary>
    public interface IMainLogic
    {
        /// <summary>
        /// This method calls the delete function of API.
        /// </summary>
        /// <param name="employeeVM">The employee to delete.</param>
        void ApiDelCar(EmployeeVM employeeVM);

        /// <summary>
        /// This function gets the employees from the API.
        /// </summary>
        /// <returns>The list of employees.</returns>
        IList<EmployeeVM> ApiGetEmployees();

        /// <summary>
        /// This method calls the edit function of API.
        /// </summary>
        /// <param name="employee">The employee to edit.</param>
        /// <param name="editorFunc">The func for edit.</param>
        void EditEmployee(EmployeeVM employee, Func<EmployeeVM, bool> editorFunc);
    }
}