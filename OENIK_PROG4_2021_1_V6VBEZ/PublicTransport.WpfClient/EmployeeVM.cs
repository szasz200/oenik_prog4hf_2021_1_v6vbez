﻿namespace PublicTransport.WpfClient
{
    using System;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// This class represent an employee view model.
    /// </summary>
    public class EmployeeVM : ObservableObject
    {
        private string employeeId;
        private JobType type;
        private string name;
        private DateTime dateOfBirth;
        private DateTime hireDate;
        private int wage;
        private bool isAdd;

        /// <summary>
        /// Gets the all job types as an array.
        /// </summary>
        public static Array JobTypes
        {
            get { return Enum.GetValues(typeof(JobType)); }
        }

        /// <summary>
        /// Gets or sets the EmployeeID of an Employee entity, which is a identifier of an employee.
        /// </summary>
        public string EmployeeId
        {
            get { return this.employeeId; }
            set { this.Set(ref this.employeeId, value); }
        }

        /// <summary>
        /// Gets or sets the Type of an Employee entity, which indicates the job type of the employee.
        /// </summary>
        public JobType Type
        {
            get { return this.type; }
            set { this.Set(ref this.type, value); }
        }

        /// <summary>
        /// Gets or sets the Name of an Employee entity.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets the Date of birth of an Employee entity.
        /// </summary>
        public DateTime DateOfBirth
        {
            get { return this.dateOfBirth; }
            set { this.Set(ref this.dateOfBirth, value); }
        }

        /// <summary>
        /// Gets or sets the HireDate of an Employee entity, which indicates the date when the employee started working for the company.
        /// </summary>
        public DateTime HireDate
        {
            get { return this.hireDate; }
            set { this.Set(ref this.hireDate, value); }
        }

        /// <summary>
        /// Gets or sets the wage of an Employee entity, which indicates the salary of the employee.
        /// </summary>
        public int Wage
        {
            get { return this.wage; }
            set { this.Set(ref this.wage, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether gets the window is in add mode or not.
        /// </summary>
        public bool IsAdd
        {
            get { return this.employeeId == null; }
            set { this.Set(ref this.isAdd, value); }
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Employee Id:{this.EmployeeId}, Name:{this.Name}, Date of birth: {this.DateOfBirth.ToShortDateString()}, Type:{this.Type}, Hire date:{this.HireDate.ToShortDateString()}, Wage:{this.Wage} ;";
        }

        /// <summary>
        /// Creates a copy from the "other" Employee.
        /// </summary>
        /// <param name="other">An Employee instance filled with datas.</param>
        public void CopyFrom(EmployeeVM other)
        {
            if (other is null)
            {
                return;
            }

            this.EmployeeId = other.EmployeeId;
            this.DateOfBirth = other.DateOfBirth;
            this.HireDate = other.HireDate;
            this.IsAdd = other.IsAdd;
            this.Name = other.Name;
            this.Type = other.Type;
            this.Wage = other.Wage;
        }
    }
}
