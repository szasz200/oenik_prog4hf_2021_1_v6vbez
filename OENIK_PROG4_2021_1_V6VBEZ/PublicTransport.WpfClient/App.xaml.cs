﻿namespace PublicTransport.WpfClient
{
    using System.Windows;
    using CommonServiceLocator;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => MyIoc.Instance);
            MyIoc.Instance.Register<IMainLogic, MainLogic>();
        }
    }
}
