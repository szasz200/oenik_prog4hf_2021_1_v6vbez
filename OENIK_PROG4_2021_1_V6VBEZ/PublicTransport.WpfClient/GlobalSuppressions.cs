﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "Will be disposed az end of the runtime.")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "It is not a commercial application.")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "The modifying is necesary.", Scope = "member", Target = "~P:PublicTransport.WpfClient.MainVM.AllEmployees")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "Will be disposed az end of the runtime.", Scope = "member", Target = "~M:PublicTransport.WpfClient.MainLogic.ApiEditEmployee(PublicTransport.WpfClient.EmployeeVM,System.Boolean)~System.Boolean")]
