﻿namespace PublicTransport.WpfClient
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// The view model of the main window.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private IMainLogic logic;
        private EmployeeVM selectedEmployee;
        private ObservableCollection<EmployeeVM> allEmployees;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IMainLogic>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        /// <param name="mainLogic">The implemetation of the logic.</param>
        public MainVM(IMainLogic mainLogic)
        {
            this.logic = mainLogic;

            this.LoadCmd = new RelayCommand(() => this.AllEmployees = new ObservableCollection<EmployeeVM>(this.logic.ApiGetEmployees()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelCar(this.SelectedEmployee));
            this.AddCmd = new RelayCommand(() => this.logic.EditEmployee(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() =>
            {
                if (this.SelectedEmployee != null)
{
    this.logic.EditEmployee(this.SelectedEmployee, this.EditorFunc);
}
            });
        }

        /// <summary>
        /// Gets or sets the list of employees.
        /// </summary>
        public ObservableCollection<EmployeeVM> AllEmployees
        {
            get { return this.allEmployees; }
            set { this.Set(ref this.allEmployees, value); }
        }

        /// <summary>
        /// Gets or sets the selected employee.
        /// </summary>
        public EmployeeVM SelectedEmployee
        {
            get { return this.selectedEmployee; }
            set { this.Set(ref this.selectedEmployee, value); }
        }

        /// <summary>
        /// Gets or sets the Func for edit employees.
        /// </summary>
        public Func<EmployeeVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets the employee add command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets the employee delete command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets the employee modify command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets the employee load command.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
