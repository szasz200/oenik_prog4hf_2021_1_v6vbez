﻿namespace PublicTransport.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net.Http;
    using System.Text.Json;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// This class invokes the API from the MVC.
    /// </summary>
    public class MainLogic : IMainLogic
    {
        private string url = "http://localhost:51459/PublicTransportAPI/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonSerializerOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <inheritdoc/>
        public IList<EmployeeVM> ApiGetEmployees()
        {
            string json = this.client.GetStringAsync(new Uri(this.url + "all")).Result;
            var list = JsonSerializer.Deserialize<List<EmployeeVM>>(json, this.jsonSerializerOptions);
            return list;
        }

        /// <inheritdoc/>
        public void ApiDelCar(EmployeeVM employeeVM)
        {
            bool success = false;
            if (employeeVM != null)
            {
                string json = this.client.GetStringAsync(new Uri(this.url + "del/" + employeeVM.EmployeeId.ToString())).Result;
                JsonDocument jsonDocument = JsonDocument.Parse(json);
                success = jsonDocument.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            SendMessage(success);
        }

        /// <inheritdoc/>
        public void EditEmployee(EmployeeVM employee, Func<EmployeeVM, bool> editorFunc)
        {
            EmployeeVM clone = new EmployeeVM();
            if (employee != null)
            {
                clone.CopyFrom(employee);
            }

            bool? success = editorFunc?.Invoke(clone);
            if (success == true)
            {
                if (employee != null)
                {
                    success = this.ApiEditEmployee(clone, true);
                }
                else
                {
                    clone.IsAdd = true;
                    success = this.ApiEditEmployee(clone, false);
                }
            }

            SendMessage(success == true);
        }

        private static void SendMessage(bool success)
        {
            string msg = success ? "Completed successfully" : "Failed";
            Messenger.Default.Send(msg, "Employee result");
        }

        private bool ApiEditEmployee(EmployeeVM employee, bool isEditing)
        {
            if (employee == null)
            {
                return false;
            }

            string myurl = this.url + (isEditing ? "mod" : "add");

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (!isEditing)
            {
                postData.Add("HireDate", employee.HireDate.ToString(CultureInfo.InvariantCulture));
                postData.Add("DateOfBirth", employee.DateOfBirth.ToString(CultureInfo.InvariantCulture));
            }

            postData.Add("EmployeeId", employee.EmployeeId);
            postData.Add("name", employee.Name);
            postData.Add("position", employee.Type.ToString());
            postData.Add("wage", employee.Wage.ToString(CultureInfo.InvariantCulture));
            string json = this.client.PostAsync(new Uri(myurl), new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JsonDocument jsonDocument = JsonDocument.Parse(json);
            return jsonDocument.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }
    }
}
