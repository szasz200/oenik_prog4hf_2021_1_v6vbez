﻿namespace PublicTransport.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// This class represents a record in the Maintenances table.
    /// </summary>
    [Table("Maintenances")]
    public partial class Maintenance
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Maintenance"/> class.
        /// </summary>
        public Maintenance()
        {
            Vehicles = new HashSet<Vehicle>();
        }

        /// <summary>
        /// Gets or sets the MaintenanceID of a Maintenance entity, which is a identifier of a maintenance.
        /// </summary>
        public int MaintenanceId { get; set; }

        /// <summary>
        /// Gets or sets the Date of a Maintenance entity, which indicates the date when the maintenance was performed.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the Lpnumber of a Maintenance entity, which indicates the vehicle which was maintained.
        /// </summary>
        public string Lpnumber { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeId of a Maintenance entity, which indicates the employee who performed the maintenance.
        /// </summary>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the Cost of a Maintenance entity, which indicates the cost of the maintenance.
        /// </summary>
        public int? Cost { get; set; }

        /// <summary>
        /// Gets or sets the Operations of a Maintenance entity, which describes the operations performed during maintenance.
        /// </summary>
        public string Operations { get; set; }

        /// <summary>
        /// Gets or sets the Employee entity from the Employees table, based on the EmployeeId.
        /// </summary>
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Gets or sets the Vechicle entity from the Vechicles table, based on the Lpnumber.
        /// </summary>
        public virtual Vehicle LpnumberNavigation { get; set; }

        /// <summary>
        /// Gets collection of Vehicles, where the Maintenance entity is a foreign key on the Vehicles table. This collection will contain only one vechicle entity() or null, depending on whether additional maintenance has been performed after maintenance or not.
        /// </summary>
        public virtual ICollection<Vehicle> Vehicles { get; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Maintenance id:{MaintenanceId}, Date:{Date.ToShortDateString()}, License plate number:{Lpnumber}, Employee id:{EmployeeId}, Cost:{Cost}, Operations:{Operations} ;";
        }
    }
}
