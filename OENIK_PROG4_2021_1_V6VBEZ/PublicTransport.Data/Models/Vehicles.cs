﻿namespace PublicTransport.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// This class represents a record in the Vehicles table.
    /// </summary>
    [Table("Vehicles")]
    public partial class Vehicle
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Vehicle"/> class.
        /// </summary>
        public Vehicle()
        {
            Maintenances = new HashSet<Maintenance>();
            WorkingDates = new HashSet<WorkingDate>();
        }

        /// <summary>
        /// Gets or sets the Lpnumber of a Vehicle entity, which is the license plate number of the vehicle as the identifier of a vehicle.
        /// </summary>
        public string Lpnumber { get; set; }

        /// <summary>
        /// Gets or sets the Type of a Vehicle entity, which indicates the type(bus, troley or tram) of the vehicle.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the YearOfManufacture of a Vehicle entity, which indicates the the year of manufacture of a vehicle.
        /// </summary>
        public int YearOfManufacture { get; set; }

        /// <summary>
        /// Gets or sets the TraveledDistance of a Vehicle entity, which indicates the position of the odometer.
        /// </summary>
        public decimal TraveledDistance { get; set; }

        /// <summary>
        /// Gets or sets the LastMaintenanceId of a Vehicle entity, which is a foreign key to a record from the Maintenances table, and indicates the last maintenance. If the vehicle is new, the LastMaintenanceId is null.
        /// </summary>
        public int? LastMaintenanceId { get; set; }

        /// <summary>
        /// Gets or sets the NextMaintenance of a Vehicle entity, which indicates the date, when the vehicle needs to be inspected.
        /// </summary>
        public DateTime? NextMaintenance { get; set; }

        /// <summary>
        /// Gets or sets the Maintenance entity from the Maintenances table, based on the LastMaintenanceId.
        /// </summary>
        public virtual Maintenance LastMaintenance { get; set; }

        /// <summary>
        /// Gets collection of Maintenances, where the vehicle is a foreign key on the Maintenances table. If the vehicle is new, this collection is null.
        /// </summary>
        public virtual ICollection<Maintenance> Maintenances { get; }

        /// <summary>
        /// Gets collection of WorkingDates, where the vehicle is a foreign key on the WorkingDates table.
        /// </summary>
        public virtual ICollection<WorkingDate> WorkingDates { get; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"License plate number:{Lpnumber}, Type:{Type}, Year of manufacture:{YearOfManufacture}, Traveled distance:{TraveledDistance}km, LastMaintenanceId:{LastMaintenanceId}, Next maintenance date:{NextMaintenance.GetValueOrDefault().ToShortDateString()} ;";
        }
    }
}
