﻿namespace PublicTransport.Data.Models
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata;

    /// <summary>
    /// Represents the PTDatabase, based on the DbContext class.
    /// </summary>
    public partial class PTDBContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PTDBContext"/> class.
        /// </summary>
        public PTDBContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PTDBContext"/> class.
        /// </summary>
        /// <param name="options">Options to the DbContext.</param>
        public PTDBContext(DbContextOptions<PTDBContext> options)
            : base(options)
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets Employees table, with Employee records.
        /// </summary>
        public virtual DbSet<Employee> Employees { get; set; }

        /// <summary>
        /// Gets or sets Lines table, with Line records.
        /// </summary>
        public virtual DbSet<Line> Lines { get; set; }

        /// <summary>
        /// Gets or sets Maintenances table, with Maintenece records.
        /// </summary>
        public virtual DbSet<Maintenance> Maintenances { get; set; }

        /// <summary>
        /// Gets or sets Vechicles table, with Vehicle records.
        /// </summary>
        public virtual DbSet<Vehicle> Vehicles { get; set; }

        /// <summary>
        /// Gets or sets WorkingDates table, with WorkingDate records.
        /// </summary>
        public virtual DbSet<WorkingDate> WorkingDates { get; set; }

        /// <inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.
                    UseLazyLoadingProxies().
                    UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename =|DataDirectory|\PTDatabase.mdf;Integrated Security=True;MultipleActiveResultSets=True");
            }
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder != null)
            {
                modelBuilder.Entity<Employee>(entity =>
                {
                    entity.HasKey(e => e.EmployeeId)
                        .HasName("Employee_pk");

                    entity.Property(e => e.EmployeeId)
                        .HasColumnName("EmployeeID")
                        .HasMaxLength(8)
                        .IsUnicode(false)
                        .IsFixedLength();

                    entity.Property(e => e.DateOfBirth).HasColumnType("date");

                    entity.Property(e => e.HireDate).HasColumnType("date");

                    entity.Property(e => e.Name)
                        .IsRequired()
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    entity.Property(e => e.Type)
                        .IsRequired()
                        .HasMaxLength(20)
                        .IsUnicode(false);

                    entity.Property(e => e.Wage).HasColumnType("numeric(7)");
                });

                modelBuilder.Entity<Line>(entity =>
                {
                    entity.HasKey(e => e.LineId)
                        .HasName("Line_pk");

                    entity.Property(e => e.LineId)
                        .HasColumnName("LineID")
                        .HasMaxLength(4)
                        .IsUnicode(false);

                    entity.Property(e => e.Length).HasColumnType("numeric(3, 1)");

                    entity.Property(e => e.NumberOfStops).HasColumnType("numeric(3)");

                    entity.Property(e => e.NumberOfVehicles).HasColumnType("numeric(3)");

                    entity.Property(e => e.TimeRequired).HasColumnType("numeric(3)");

                    entity.Property(e => e.Type)
                        .IsRequired()
                        .HasMaxLength(8)
                        .IsUnicode(false);
                });

                modelBuilder.Entity<Maintenance>(entity =>
                {
                    entity.HasKey(e => e.MaintenanceId)
                        .HasName("Kabantartasok_pk");

                    entity.Property(e => e.MaintenanceId)
                        .HasColumnName("MaintenanceID")
                        .HasColumnType("numeric(11)")
                        .ValueGeneratedOnAdd();

                    entity.Property(e => e.Cost).HasColumnType("numeric(9)");

                    entity.Property(e => e.Date).HasColumnType("date");

                    entity.Property(e => e.EmployeeId)
                        .IsRequired()
                        .HasColumnName("EmployeeID")
                        .HasMaxLength(8)
                        .IsUnicode(false)
                        .IsFixedLength();

                    entity.Property(e => e.Lpnumber)
                        .IsRequired()
                        .HasColumnName("LPNumber")
                        .HasMaxLength(7)
                        .IsUnicode(false);

                    entity.Property(e => e.Operations)
                        .IsRequired()
                        .HasMaxLength(250)
                        .IsUnicode(false);

                    entity.HasOne(d => d.Employee)
                        .WithMany(p => p.Maintenances)
                        .HasForeignKey(d => d.EmployeeId)
                        .OnDelete(DeleteBehavior.Cascade)
                        .HasConstraintName("Kalakamazott_pk");

                    entity.HasOne(d => d.LpnumberNavigation)
                        .WithMany(p => p.Maintenances)
                        .HasForeignKey(d => d.Lpnumber)
                        .OnDelete(DeleteBehavior.Cascade)
                        .HasConstraintName("Kjarmu_pk");
                });

                modelBuilder.Entity<Vehicle>(entity =>
                {
                    entity.HasKey(e => e.Lpnumber)
                        .HasName("Jarmu_pk");

                    entity.Property(e => e.Lpnumber)
                        .HasColumnName("LPNumber")
                        .HasMaxLength(7)
                        .IsUnicode(false);

                    entity.Property(e => e.LastMaintenanceId)
                        .HasColumnName("LastMaintenanceID")
                        .HasColumnType("numeric(11)");

                    entity.Property(e => e.NextMaintenance).HasColumnType("date");

                    entity.Property(e => e.TraveledDistance).HasColumnType("numeric(8, 1)");

                    entity.Property(e => e.Type)
                        .IsRequired()
                        .HasMaxLength(8)
                        .IsUnicode(false);

                    entity.Property(e => e.YearOfManufacture).HasColumnType("numeric(4)");

                    entity.HasOne(d => d.LastMaintenance)
                        .WithMany(p => p.Vehicles)
                        .HasForeignKey(d => d.LastMaintenanceId)
                        .HasConstraintName("uk_fk");
                });

                modelBuilder.Entity<WorkingDate>(entity =>
                {
                    entity.HasKey(e => e.WorkingId)
                        .HasName("Szolgalat_pk");

                    entity.Property(e => e.WorkingId)
                        .HasColumnName("WorkingID")
                        .HasColumnType("numeric(11)")
                        .ValueGeneratedOnAdd();

                    entity.Property(e => e.Consumption).HasColumnType("numeric(4)");

                    entity.Property(e => e.Date).HasColumnType("date");

                    entity.Property(e => e.EmployeeId)
                        .IsRequired()
                        .HasColumnName("EmployeeID")
                        .HasMaxLength(8)
                        .IsUnicode(false)
                        .IsFixedLength();

                    entity.Property(e => e.LineId)
                        .IsRequired()
                        .HasColumnName("LineID")
                        .HasMaxLength(3)
                        .IsUnicode(false);

                    entity.Property(e => e.Lpnumber)
                        .IsRequired()
                        .HasColumnName("LPNumber")
                        .HasMaxLength(7)
                        .IsUnicode(false);

                    entity.Property(e => e.WorkingMinutes).HasColumnType("numeric(3)");

                    entity.HasOne(d => d.Employee)
                        .WithMany(p => p.WorkingDates)
                        .HasForeignKey(d => d.EmployeeId)
                        .OnDelete(DeleteBehavior.Cascade)
                        .HasConstraintName("Szalakamazott_pk");

                    entity.HasOne(d => d.Line)
                        .WithMany(p => p.WorkingDates)
                        .HasForeignKey(d => d.LineId)
                        .OnDelete(DeleteBehavior.Cascade)
                        .HasConstraintName("SzJarat_pk");

                    entity.HasOne(d => d.LpnumberNavigation)
                        .WithMany(p => p.WorkingDates)
                        .HasForeignKey(d => d.Lpnumber)
                        .OnDelete(DeleteBehavior.Cascade)
                        .HasConstraintName("Szjarmu_pk");
                });

                Employee employee1 = new Employee() { EmployeeId = "A365F2D1", DateOfBirth = new DateTime(1985, 06, 15), HireDate = new DateTime(2019, 02, 11), Name = "Gabor Kovacs", Type = "Bus driver", Wage = 2200 };
                Employee employee2 = new Employee() { EmployeeId = "F0214A58", DateOfBirth = new DateTime(1995, 11, 15), HireDate = new DateTime(2018, 09, 14), Name = "Bernadett Nagy", Type = "Tram driver", Wage = 2800 };
                Employee employee3 = new Employee() { EmployeeId = "01B14A38", DateOfBirth = new DateTime(1991, 10, 22), HireDate = new DateTime(2018, 06, 24), Name = "Ervin Molnar", Type = "Maintainer", Wage = 2700 };
                Employee employee4 = new Employee() { EmployeeId = "B0B15B99", DateOfBirth = new DateTime(1999, 07, 02), HireDate = new DateTime(2020, 07, 21), Name = "Johanna Kun", Type = "Maintainer", Wage = 2500 };
                Employee employee5 = new Employee() { EmployeeId = "12BFA515", DateOfBirth = new DateTime(1989, 12, 11), HireDate = new DateTime(2018, 03, 12), Name = "Patrik Foldesi", Type = "Accountant", Wage = 3500 };

                modelBuilder.Entity<Employee>().HasData(employee1, employee2, employee3, employee4, employee5);
            }
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
