﻿namespace PublicTransport.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// This class represents a record in the Employees table.
    /// </summary>
    [Table("Employees")]
    public partial class Employee
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Employee"/> class.
        /// </summary>
        public Employee()
        {
            Maintenances = new HashSet<Maintenance>();
            WorkingDates = new HashSet<WorkingDate>();
        }

        /// <summary>
        /// Gets or sets the EmployeeID of an Employee entity, which is a identifier of an employee (8-digit hexadecimal number).
        /// </summary>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the Type of an Employee entity, which is a 8-digit hexadecimal number and indicates the job type of the employee.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the Name of an Employee entity.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Date of birth of an Employee entity.
        /// </summary>
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the HireDate of an Employee entity, which indicates the date when the employee started working for the company.
        /// </summary>
        public DateTime HireDate { get; set; }

        /// <summary>
        /// Gets or sets the wage of an Employee entity, which indicates the salary of the employee.
        /// </summary>
        public int Wage { get; set; }

        /// <summary>
        /// Gets collection of Maintances, where the employee is a foreign key on the Maintenances table. If the employee isn't works as a maintaniner, the value of the collection is null.
        /// </summary>
        public virtual ICollection<Maintenance> Maintenances { get; }

        /// <summary>
        /// Gets collection of WorkingDates, where the employee is a foreign key on the WorkingDates table. If the employee isn't works as a driver, the value of the collection is null.
        /// </summary>
        public virtual ICollection<WorkingDate> WorkingDates { get; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Employee Id:{EmployeeId}, Name:{Name}, Date of birth: {DateOfBirth.ToShortDateString()}, Type:{Type}, Hire date:{HireDate.ToShortDateString()}, Wage:{Wage} ;";
        }
    }
}
