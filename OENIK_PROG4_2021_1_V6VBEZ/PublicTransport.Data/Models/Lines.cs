﻿namespace PublicTransport.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// This class represents a record in the Lines table.
    /// </summary>
    [Table("Lines")]
    public partial class Line
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Line"/> class.
        /// </summary>
        public Line()
        {
            WorkingDates = new HashSet<WorkingDate>();
        }

        /// <summary>
        /// Gets or sets the LineID of a Line entity, which is a identifier of a line.
        /// </summary>
        public string LineId { get; set; }

        /// <summary>
        /// Gets or sets the Type of a Line entity, which indicates the type(bus, troley or tram) of the line.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the Length of a Line entity, which indicates the length of the line in kilometers.
        /// </summary>
        public decimal Length { get; set; }

        /// <summary>
        /// Gets or sets the TimeRequired of a Line entity, which indicates the minutes, which is required to traverse the line.
        /// </summary>
        public int TimeRequired { get; set; }

        /// <summary>
        /// Gets or sets the NumberOfStop of a Line entity, which indicates the number of stop along the line.
        /// </summary>
        public int NumberOfStops { get; set; }

        /// <summary>
        /// Gets or sets the NumberOfVehicles of a Line entity, which indicates the number of vechicles on the line at the same time.
        /// </summary>
        public int NumberOfVehicles { get; set; }

        /// <summary>
        /// Gets collection of WorkingDates, where the line entity is a foreign key on the WorkingDates table.
        /// </summary>
        public virtual ICollection<WorkingDate> WorkingDates { get; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Line id:{LineId}, Line type:{Type}, Length:{Length}km, Required time:{TimeRequired}, Number of stops:{NumberOfStops}, Number of vehicles:{NumberOfVehicles} ;";
        }
    }
}
