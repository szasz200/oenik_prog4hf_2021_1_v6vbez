﻿namespace PublicTransport.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// This class represents a record in the WorkigDates table.
    /// </summary>
    [Table("WorkingDates")]
    public partial class WorkingDate
    {
        /// <summary>
        /// Gets or sets the WorkingId of a WorkingDate entity, which is a identifier of a working day.
        /// </summary>
        public int WorkingId { get; set; }

        /// <summary>
        /// Gets or sets the Date of a WorkingDate entity, which indicates the date of the working day.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the LineId of a WorkingDate entity, which is a lineId form the Lines table.
        /// </summary>
        public string LineId { get; set; }

        /// <summary>
        /// Gets or sets the Lpnumber of a WorkingDate entity, which is a Lpnumber form the Vehicles table and indicates which vehicle was traveling.
        /// </summary>
        public string Lpnumber { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeId of a WorkingDate entity, which is a EmployeeId form the Employees table and indicates which employee worked.
        /// </summary>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the WorkingMinutes of a WorkingDate entity, which indicates number of minutes which the employee worked that day.
        /// </summary>
        public int WorkingMinutes { get; set; }

        /// <summary>
        /// Gets or sets the Consumption of a WorkingDate entity, which indicates the amount of consumed electricity or fuel.
        /// </summary>
        public int Consumption { get; set; }

        /// <summary>
        /// Gets or sets the Employee entity from the Employees table, based on the EmployeeId.
        /// </summary>
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Gets or sets the Line entity from the Lines table, based on the LineId.
        /// </summary>
        public virtual Line Line { get; set; }

        /// <summary>
        /// Gets or sets the Vechicle entity from the Vechicles table, based on the Lpnumber.
        /// </summary>
        public virtual Vehicle LpnumberNavigation { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Workdate id:{WorkingId}, Date:{Date.ToShortDateString()}, Employee:{EmployeeId}, Vehicle:{Lpnumber}, Line:{LineId}, Worked minutes:{WorkingMinutes}, Consumption:{Consumption} ;";
        }
    }
}
