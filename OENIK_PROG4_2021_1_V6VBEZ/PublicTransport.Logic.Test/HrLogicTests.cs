﻿namespace PublicTransport.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit.Framework;
    using PublicTransport.Data.Models;
    using PublicTransport.Repository;

    /// <summary>
    /// It is a test class for the Human Resource Logic.
    /// </summary>
    [TestFixture]
    public class HrLogicTests
    {
        /// <summary>
        /// Tests the "GetAll" repository method, by humanresource logic.
        /// </summary>
        [Test]
        public void TestGetByPosition()
        {
            // Arrange
            Mock<IEmployeeRepository> mockedEmployees = new Mock<IEmployeeRepository>(MockBehavior.Loose);
            List<Employee> employees = new List<Employee>()
            {
                new Employee() { EmployeeId = "ABCD1234", Name = "Jack", Type = "Bus driver" },
                new Employee() { EmployeeId = "FADC1769", Name = "Nevil", Type = "Maintainer" },
                new Employee() { EmployeeId = "CDEF4321", Name = "Angelina", Type = "Tram driver" },
                new Employee() { EmployeeId = "FABC5789", Name = "Peter", Type = "Maintainer" },
            };
            List<Employee> expectedEmployees = new List<Employee>() { employees[1], employees[3] };
            mockedEmployees.Setup(repo => repo.GetAll()).Returns(employees.AsQueryable());
            HumanResourceLogic hrlogic = new HumanResourceLogic(mockedEmployees.Object);

            // Act
            var result = hrlogic.GetByPosition("Maintainer");

            // Verify
            mockedEmployees.Verify(repo => repo.GetAll(), Times.Once);
            mockedEmployees.Verify(repo => repo.GetOne(It.IsAny<string>()), Times.Never);
        }
    }
}
