﻿namespace PublicTransport.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit.Framework;
    using PublicTransport.Data.Models;
    using PublicTransport.Repository;

    /// <summary>
    /// It is a test class for the Accountant Logic.
    /// </summary>
    [TestFixture]
    public class AccountatLogicTest
    {
        /// <summary>
        /// Tests the "NewRecord" repository method, by accountant logic.
        /// </summary>
        [Test]
        public void TestNewWorkDate()
        {
            Mock<IWorkingDateRepository> mockedWorkingDates = new Mock<IWorkingDateRepository>();
            mockedWorkingDates.Setup(repo => repo.NewRecord(It.IsAny<WorkingDate>()));
            AccountantLogic logic = new AccountantLogic(mockedWorkingDates.Object, null, null);
            WorkingDate workingDate1 = new WorkingDate() { WorkingId = 1 };
            logic.NewWorkDate(workingDate1);
            mockedWorkingDates.Verify(repo => repo.NewRecord(workingDate1), Times.Once);
        }

        /// <summary>
        /// Tests the "GetDieselConsumption" accountant logic method.
        /// </summary>
        [Test]
        public void TestGetDieselConsumption()
        {
            Mock<IWorkingDateRepository> mockedWorkingDates = new Mock<IWorkingDateRepository>();
            Mock<IEmployeeRepository> mockedEmployees = new Mock<IEmployeeRepository>();
            List<WorkingDate> workingDates = new List<WorkingDate>()
            {
                new WorkingDate() { Consumption = 35, Date = new DateTime(2020, 12, 04), EmployeeId = "ABCD1234" },
                new WorkingDate() { Consumption = 21, Date = new DateTime(2020, 12, 03), EmployeeId = "ABCD1234" },
                new WorkingDate() { Consumption = 40, Date = new DateTime(2020, 12, 03), EmployeeId = "CDEF1234" },
                new WorkingDate() { Consumption = 1026, Date = new DateTime(2020, 12, 03), EmployeeId = "ABCD5678" },
            };
            List<Employee> employees = new List<Employee>()
            {
                new Employee() { EmployeeId = "ABCD1234", Type = "Bus driver" },
                new Employee() { EmployeeId = "CDEF1234", Type = "Bus driver" },
                new Employee() { EmployeeId = "ABCD5678", Type = "Tram driver" },
            };
            mockedWorkingDates.Setup(repo => repo.GetAll()).Returns(workingDates.AsQueryable());
            mockedEmployees.Setup(repo => repo.GetAll()).Returns(employees.AsQueryable());
            AccountantLogic logic = new AccountantLogic(mockedWorkingDates.Object, null, mockedEmployees.Object);
            var result = logic.GetDieselConsumption(12);
            int expectedresult = 40 + 21 + 35;
            Assert.That(result, Is.EqualTo(expectedresult));
            mockedEmployees.Verify(repo => repo.GetAll(), Times.Once);
            mockedWorkingDates.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests the "GetEmployeeworkingMinutes" accountant logic method.
        /// </summary>
        [Test]
        public void TestGetEmployeeworkingMinutes()
        {
            Mock<IWorkingDateRepository> mockedWorkingDates = new Mock<IWorkingDateRepository>();
            Mock<IEmployeeRepository> mockedEmployees = new Mock<IEmployeeRepository>();
            List<WorkingDate> workingDates = new List<WorkingDate>()
            {
                new WorkingDate() { WorkingMinutes = 476, Date = new DateTime(2020, 12, 04), EmployeeId = "ABCD1234" },
                new WorkingDate() { WorkingMinutes = 481, Date = new DateTime(2020, 12, 03), EmployeeId = "ABCD1234" },
                new WorkingDate() { WorkingMinutes = 446, Date = new DateTime(2020, 12, 03), EmployeeId = "CDEF1234" },
                new WorkingDate() { WorkingMinutes = 490, Date = new DateTime(2020, 12, 03), EmployeeId = "ABCD5678" },
            };
            List<Employee> employees = new List<Employee>()
            {
                new Employee() { EmployeeId = "ABCD1234", Name = "Jack", Type = "Bus driver" },
                new Employee() { EmployeeId = "CDEF1234", Name = "Elisabet", Type = "Bus driver" },
                new Employee() { EmployeeId = "ABCD5678", Name = "John", Type = "Tram driver" },
            };
            mockedWorkingDates.Setup(repo => repo.GetAll()).Returns(workingDates.AsQueryable());
            mockedEmployees.Setup(repo => repo.GetAll()).Returns(employees.AsQueryable());
            AccountantLogic logic = new AccountantLogic(mockedWorkingDates.Object, null, mockedEmployees.Object);
            var result = logic.GetEmployeeWorkingMinutes(12);
            var expectedresult = new List<WorkHours>()
            {
                new WorkHours() { EmpolyeeID = "ABCD1234", EmpoloyeeName = "Jack", WorkedHours = (476 + 481) / 60 },
                new WorkHours() { EmpolyeeID = "CDEF1234", EmpoloyeeName = "Elisabet", WorkedHours = 446 / 60 },
                new WorkHours() { EmpolyeeID = "ABCD5678", EmpoloyeeName = "John", WorkedHours = 490 / 60 },
            };
            Assert.That(result, Is.EqualTo(expectedresult));
            mockedEmployees.Verify(repo => repo.GetAll(), Times.Once);
            mockedWorkingDates.Verify(repo => repo.GetAll(), Times.Once);
        }
    }
}
