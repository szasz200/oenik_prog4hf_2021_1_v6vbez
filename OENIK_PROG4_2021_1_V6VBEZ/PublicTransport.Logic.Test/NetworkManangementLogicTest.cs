﻿namespace PublicTransport.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Moq;
    using NUnit.Framework;
    using PublicTransport.Data.Models;
    using PublicTransport.Repository;

    /// <summary>
    /// It is a test class for the Network Management Logic.
    /// </summary>
    [TestFixture]
    public class NetworkManangementLogicTest
    {
        /// <summary>
        /// Tests the "UpdateNumberOfStops" repository method, by network manangement logic.
        /// </summary>
        [Test]
        public void UpdateNumberOfStops()
        {
            Mock<ILineRepository> mockedLines = new Mock<ILineRepository>();
            mockedLines.Setup(repo => repo.UpdateNumberOfStops(It.IsAny<string>(), It.IsAny<int>()));
            NetworkManagementLogic logic = new NetworkManagementLogic(mockedLines.Object);
            logic.ChangeNumberOfStops("17", 25);
            mockedLines.Verify(repo => repo.UpdateNumberOfStops("17", 25), Times.Once);
        }

        /// <summary>
        /// Tests the "AvarageLineTypeLengths" network manangement logic method.
        /// </summary>
        [Test]
        public void TestAvarageLineTypeLengths()
        {
            Mock<ILineRepository> mockedLines = new Mock<ILineRepository>();
            List<Line> lines = new List<Line>()
            {
                new Line() { Type = "Tram", Length = 15 },
                new Line() { Type = "Bus", Length = 8 },
                new Line() { Type = "Bus", Length = 12 },
                new Line() { Type = "Tram", Length = 14 },
                new Line() { Type = "Trolley", Length = 3 },
                new Line() { Type = "Tram", Length = 11 },
            };
            mockedLines.Setup(repo => repo.GetAll()).Returns(lines.AsQueryable());
            NetworkManagementLogic logic = new NetworkManagementLogic(mockedLines.Object);
            var result = logic.AvarageLineTypeLengths();
            var expectedresult = new List<TypeAvarageLength>()
            {
                new TypeAvarageLength() { LineType = VType.Bus, LineTypeLength = (decimal)(12 + 8) / 2 },
                new TypeAvarageLength() { LineType = VType.Tram, LineTypeLength = (decimal)(14 + 15 + 11) / 3 },
                new TypeAvarageLength() { LineType = VType.Trolley, LineTypeLength = 3 },
            };
            Assert.That(result, Is.EquivalentTo(expectedresult));
            mockedLines.Verify(repo => repo.GetAll(), Times.Once);
        }
    }
}
