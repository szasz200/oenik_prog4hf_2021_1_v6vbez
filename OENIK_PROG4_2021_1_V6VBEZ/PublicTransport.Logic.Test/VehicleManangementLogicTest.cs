﻿namespace PublicTransport.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Moq;
    using NUnit.Framework;
    using PublicTransport.Data.Models;
    using PublicTransport.Repository;

    /// <summary>
    /// It is a test class for the Vehicle Manangement logic.
    /// </summary>
    [TestFixture]
    public class VehicleManangementLogicTest
    {
        /// <summary>
        /// Tests the "GetOne" repository method, by vehicle manangement logic.
        /// </summary>
        [Test]
        public void TestGetVehicle()
        {
            // Arrange
            Mock<IVehicleRepository> mockedVehicles = new Mock<IVehicleRepository>();
            List<Vehicle> vehicles = new List<Vehicle>()
            {
                new Vehicle() { Lpnumber = "BPO-123", Type = "Bus" },
                new Vehicle() { Lpnumber = "2365", Type = "Tram" },
                new Vehicle() { Lpnumber = "BPO-321", Type = "Bus" },
            };
            mockedVehicles.Setup(repo => repo.GetOne(It.IsAny<string>())).Returns(vehicles[0]);
            VehicleManagementLogic logic = new VehicleManagementLogic(mockedVehicles.Object, null);
            Vehicle vehicle = logic.GetVehicle("BPO-123");

            // Verify
            mockedVehicles.Verify(repo => repo.GetOne("BPO-123"), Times.Once);
            mockedVehicles.Verify(repo => repo.GetAll(), Times.Never);
        }

        /// <summary>
        /// Tests the "RemoveVehicle" repository method, by vehicle manangement logic.
        /// </summary>
        [Test]
        public void RemoveVehicle()
        {
            Mock<IVehicleRepository> mockedVehicles = new Mock<IVehicleRepository>();
            mockedVehicles.Setup(repo => repo.DeleteOne(It.IsAny<string>()));
            VehicleManagementLogic logic = new VehicleManagementLogic(mockedVehicles.Object, null);
            logic.RemoveVehicle("BPO-123");
            mockedVehicles.Verify(repo => repo.DeleteOne("BPO-123"), Times.Once);
        }
    }
}
