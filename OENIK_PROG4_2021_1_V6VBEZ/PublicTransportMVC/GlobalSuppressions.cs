﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "It is not a commercial application.")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "The writing of list is necesarry.", Scope = "member", Target = "~P:PublicTransport.MVC.Models.EmployeeListViewModel.Employees")]
[assembly: SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "I want catch the all exception in this case, because the edit was not successful.", Scope = "member", Target = "~M:PublicTransport.MVC.Controllers.EmployeesController.Edit(PublicTransport.MVC.Models.Employee,System.String)~Microsoft.AspNetCore.Mvc.IActionResult")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA0001:XML comment analysis is disabled due to project  configuration", Justification = "It is an MVC application.")]
[assembly: SuppressMessage("Build", "CA1014:Mark assemblies with CLSCompliant", Justification = "It is an MVC application.")]
