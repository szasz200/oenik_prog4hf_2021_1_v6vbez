﻿namespace PublicTransport.MVC.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using PublicTransport.Logic;

    /// <summary>
    /// The controller class of the API.
    /// </summary>
    public partial class PublicTransportAPIController : Controller
    {
        private IHumanResourceLogic humanResourceLogic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="PublicTransportAPIController"/> class.
        /// </summary>
        /// <param name="humanResourceLogic">The implementatuin of a HumanResourceLogic.</param>
        /// <param name="mapper">The mapper.</param>
        public PublicTransportAPIController(IHumanResourceLogic humanResourceLogic, IMapper mapper)
        {
            this.humanResourceLogic = humanResourceLogic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Gets the all employees from the logic.
        /// </summary>
        /// <returns>The list of employees.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Employee> GetAll()
        {
            var employees = this.humanResourceLogic.GetAllEmployee();
            return this.mapper.Map<IList<Data.Models.Employee>, List<Models.Employee>>(employees);
        }

        /// <summary>
        /// This function removes an employee from the logic.
        /// </summary>
        /// <param name="id">The id of the employee.</param>
        /// <returns>True if the remove was successful, false if not.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DelOneEmployee(string id)
        {
            return new ApiResult() { OperationResult = this.humanResourceLogic.RemoveEmployee(id) };
        }

        /// <summary>
        /// This fuction adds a new employee to the logic.
        /// </summary>
        /// <param name="employee">The new employee filled with data.</param>
        /// <returns>True if the add was successful, false if not.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneEmployee(Models.Employee employee)
        {
            bool success = true;
            try
            {
                this.humanResourceLogic.NewEmployee(this.mapper.Map<MVC.Models.Employee, Data.Models.Employee>(employee));
            }
            catch (ArgumentException)
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// This fuction modifies an employee in the logic.
        /// </summary>
        /// <param name="employee">The new employee filled with data.</param>
        /// <returns>True if the add was successful, false if not.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneEmployee(Models.Employee employee)
        {
            bool success = true;
            if (employee is null)
            {
                return new ApiResult() { OperationResult = false };
            }

            try
            {
                this.humanResourceLogic.ChangeName(employee.EmployeeId, employee.Name);
                this.humanResourceLogic.ChangePositon(employee.EmployeeId, this.mapper.Map<MVC.Models.Employee, Data.Models.Employee>(employee).Type);
                this.humanResourceLogic.ChangeWage(employee.EmployeeId, employee.Wage);
            }
            catch (ArgumentException)
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }
    }
}
