﻿namespace PublicTransport.MVC.Controllers
{
    using System.Diagnostics;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using PublicTransport.MVC.Models;

    /// <summary>
    /// The base controller.
    /// </summary>
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="logger">An ILogger implementation.</param>
        public HomeController(ILogger<HomeController> logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Gets the base View.
        /// </summary>
        /// <returns>The base View.</returns>
        public IActionResult Index()
        {
            return this.View();
        }

        /// <summary>
        /// Gets the base View.
        /// </summary>
        /// <returns>The base View.</returns>
        public IActionResult Privacy()
        {
            return this.View();
        }

        /// <summary>
        /// This method displays the errors.
        /// </summary>
        /// <returns>Returs a View with parameters of th error.</returns>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return this.View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? this.HttpContext.TraceIdentifier });
        }
    }
}
