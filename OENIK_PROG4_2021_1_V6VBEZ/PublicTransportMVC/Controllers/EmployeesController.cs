﻿namespace PublicTransport.MVC.Controllers
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using PublicTransport.Logic;
    using PublicTransport.MVC.Models;

    /// <summary>
    /// The MVC controller class of employees.
    /// </summary>
    public class EmployeesController : Controller
    {
        private IHumanResourceLogic logic;
        private IMapper mapper;
        private EmployeeListViewModel viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeesController"/> class.
        /// </summary>
        /// <param name="logic">An IHumanResourceLogic implementation.</param>
        /// <param name="mapper">An IMapper implementation. </param>
        public EmployeesController(IHumanResourceLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;

            this.viewModel = new EmployeeListViewModel();
            this.viewModel.EditedEmployee = new Models.Employee();

            IList<Data.Models.Employee> employees = logic?.GetAllEmployee();
            this.viewModel.Employees = mapper?.Map<IList<Data.Models.Employee>, List<MVC.Models.Employee>>(employees);
        }

        /// <summary>
        /// The main page of the website.
        /// </summary>
        /// <returns>The EmployeesIndex view. </returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("EmployeesIndex", this.viewModel);
        }

        /// <summary>
        /// Shows the Details view.
        /// </summary>
        /// <param name="id">The identifier of the employee. </param>
        /// <returns>The EmployeesDetails view.</returns>
        // GET Employees/Details
        public IActionResult Details(string id)
        {
            return this.View("EmployeesDetails", this.GetEmployeeModel(id));
        }

        /// <summary>
        /// Removes the selected employee.
        /// </summary>
        /// <param name="id">The identifier of the employee. </param>
        /// <returns>The EmployeesIndex view. </returns>
        // GET Employees/Remove
        public IActionResult Remove(string id)
        {
            this.TempData["editResult"] = "DeleteFaild";
            if (this.logic.RemoveEmployee(id))
            {
                this.TempData["editResult"] = "Delete OK";
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// This method fils the Index page with datas.
        /// </summary>
        /// <param name="id">The identifier of the employee.</param>
        /// <returns>The EmployeesIndex view.</returns>
        // GET Employees/Edit
        public IActionResult Edit(string id)
        {
            this.ViewData["editAction"] = "Edit";
            this.viewModel.EditedEmployee = this.GetEmployeeModel(id);
            return this.View("EmployeesIndex", this.viewModel);
        }

        /// <summary>
        /// This method refresh the datas of an employee with same identifier.
        /// </summary>
        /// <param name="employee">The employe which we want to edit.</param>
        /// <param name="editAction">It specifies that we want to add or edit an employee.</param>
        /// <returns>The EmployeesIndex view.</returns>
        // POST Employees/Edit
        [HttpPost]
        public IActionResult Edit(Models.Employee employee, string editAction)
        {
            if (this.ModelState.IsValid && employee != null)
            {
                this.TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    try
                    {
                        this.logic.NewEmployee(this.mapper.Map<MVC.Models.Employee, Data.Models.Employee>(employee));
                        this.TempData["editResult"] = "Add OK";
                    }
                    catch (ArgumentException e)
                    {
                        this.TempData["editResult"] = "AddEmployee FAILED " + e.Message;
                    }
                }
                else
                {
                    try
                    {
                        var emp = this.GetEmployeeModel(employee.EmployeeId);
                        if (emp.Name != employee.Name)
                        {
                            this.logic.ChangeName(emp.EmployeeId, employee.Name);
                        }

                        if (emp.Type != employee.Type)
                        {
                            this.logic.ChangePositon(emp.EmployeeId, employee.Type.ToString());
                        }

                        if (emp.Wage != employee.Wage)
                        {
                            this.logic.ChangeWage(emp.EmployeeId, employee.Wage);
                        }
                    }
                    catch (Exception e)
                    {
                        this.TempData["editResult"] = "Edit FAILED " + e.Message;
                    }
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.viewModel.EditedEmployee = employee;
                return this.View("EmployeesIndex", this.viewModel);
            }
        }

        private Models.Employee GetEmployeeModel(string id)
        {
            Data.Models.Employee employee = this.logic.GetById(id);
            return this.mapper.Map<Data.Models.Employee, MVC.Models.Employee>(employee);
        }
    }
}
