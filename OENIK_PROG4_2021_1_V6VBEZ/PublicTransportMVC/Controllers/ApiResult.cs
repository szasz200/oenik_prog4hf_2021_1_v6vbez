﻿namespace PublicTransport.MVC.Controllers
{
    /// <summary>
    /// This class represents the result of an API fuction.
    /// </summary>
    public class ApiResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether the result of api successfully.
        /// </summary>
        public bool OperationResult { get; set; }
    }
}
