namespace PublicTransport.MVC.Models
{
    /// <summary>
    /// Contains datas for an Error View.
    /// </summary>
    public class ErrorViewModel
    {
        /// <summary>
        /// Gets or sets the indentifier of the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Gets a value indicating whether returns true if the RequestID is filed with data, false if not.
        /// </summary>
        public bool ShowRequestId => !string.IsNullOrEmpty(this.RequestId);
    }
}
