﻿namespace PublicTransport.MVC.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// This class represents an Employee in the UI.
    /// </summary>
    public class Employee
    {
        /// <summary>
        /// Gets or sets the EmployeeID of an Employee entity, which is a identifier of an employee.
        /// </summary>
        [Display(Name = "Employee ID Number")]
        [StringLength(8, MinimumLength = 8)]
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the Type of an Employee entity, which indicates the job type of the employee.
        /// </summary>
        [Display(Name = "Employee Job")]
        [Required]
        public JobType Type { get; set; }

        /// <summary>
        /// Gets or sets the Name of an Employee entity.
        /// </summary>
        [Display(Name = "Employee Name")]
        [Required]
        [StringLength(60, MinimumLength = 3)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Date of birth of an Employee entity.
        /// </summary>
        [Display(Name = "Employee Date of birth ")]
        [Required]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the HireDate of an Employee entity, which indicates the date when the employee started working for the company.
        /// </summary>
        [Display(Name = "Date of hiring ")]
        [Required]
        public DateTime HireDate { get; set; }

        /// <summary>
        /// Gets or sets the wage of an Employee entity, which indicates the salary of the employee.
        /// </summary>
        [Display(Name = "Employee Salary ")]
        [Required]
        public int Wage { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Employee Id:{this.EmployeeId}, Name:{this.Name}, Date of birth: {this.DateOfBirth.ToShortDateString()}, Type:{this.Type.ToString()}, Hire date:{this.HireDate.ToShortDateString()}, Wage:{this.Wage} ;";
        }
    }
}
