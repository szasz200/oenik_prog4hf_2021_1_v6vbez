﻿namespace PublicTransport.MVC.Models
{
    using System;
    using AutoMapper;

    /// <summary>
    /// Describes the data transformation between the Data layer and the MVC layer.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// Defindes how to create a mapp for this application.
        /// </summary>
        /// <returns>A mapper between the Data layer and the MVC layer. </returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.Models.Employee, MVC.Models.Employee>().
                    ForMember(dest => dest.EmployeeId, map => map.MapFrom(src => src.EmployeeId)).
                    ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name)).
                    ForMember(dest => dest.DateOfBirth, map => map.MapFrom(src => src.DateOfBirth)).
                    ForMember(dest => dest.HireDate, map => map.MapFrom(src => src.HireDate)).
                    ForMember(dest => dest.Wage, map => map.MapFrom(src => src.Wage)).
                    ForMember(dest => dest.Type, map => map.MapFrom(src => (JobType)Enum.Parse(typeof(JobType), src.Type, true))).
                    ReverseMap().
                    ForMember(dest => dest.Type, map => map.MapFrom(src => src.Type.ToString()));
            });
            return config.CreateMapper();
        }
    }
}
