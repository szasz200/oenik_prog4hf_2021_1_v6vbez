﻿namespace PublicTransport.MVC.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// The ViewModel of list of employees.
    /// </summary>
    public class EmployeeListViewModel
    {
        /// <summary>
        /// Gets or sets the list of employees.
        /// </summary>
        public IList<Employee> Employees { get; set; }

        /// <summary>
        /// Gets or sets the selected employee.
        /// </summary>
        public Employee EditedEmployee { get; set; }
    }
}
