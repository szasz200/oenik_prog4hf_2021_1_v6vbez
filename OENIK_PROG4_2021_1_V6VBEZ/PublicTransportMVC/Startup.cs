namespace PublicTransport.MVC
{
    using System;
    using AutoMapper;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using PublicTransport.Data.Models;
    using PublicTransport.Logic;
    using PublicTransport.MVC.Models;
    using PublicTransport.Repository;

    /// <summary>
    /// It initialize the MVC application.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">The base configuration of the application. </param>
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        /// <summary>
        /// Gets the Configuration of the app initializer.
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method add services to the container.
        /// </summary>
        /// <param name="services">The services of the IoC container. </param>
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddScoped<IHumanResourceLogic, HumanResourceLogic>();
            services.AddScoped<IEmployeeRepository, EmployeeRepository>();
            services.AddScoped<DbContext, PTDBContext>();
            services.AddSingleton<IMapper>(provider => MapperFactory.CreateMapper());
            using (PTDBContext pTDBContext = new PTDBContext())
            {
                AddData(pTDBContext);
            }
        }

        /// <summary>
        /// Configures the HTTP request pipeline.
        /// </summary>
        /// <param name="app">An IApplicationBuilder implementation.</param>
        /// <param name="env">An IWebHostEnvirronment implementation.</param>
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private static void AddData(PTDBContext pTDB)
        {
            if (pTDB.Employees.CountAsync().Result == 0)
            {
                PublicTransport.Data.Models.Employee employee1 = new PublicTransport.Data.Models.Employee() { EmployeeId = "A365F2D1", DateOfBirth = new DateTime(1985, 06, 15), HireDate = new DateTime(2019, 02, 11), Name = "G�bor Kov�cs", Type = "Busdriver", Wage = 220000 };
                PublicTransport.Data.Models.Employee employee2 = new PublicTransport.Data.Models.Employee() { EmployeeId = "F0214A58", DateOfBirth = new DateTime(1995, 11, 15), HireDate = new DateTime(2018, 09, 14), Name = "Bernadett Nagy", Type = "Tramdriver", Wage = 280000 };
                PublicTransport.Data.Models.Employee employee3 = new PublicTransport.Data.Models.Employee() { EmployeeId = "01B14A38", DateOfBirth = new DateTime(1991, 10, 22), HireDate = new DateTime(2018, 06, 24), Name = "Ervin Moln�r", Type = "Maintainer", Wage = 270000 };
                PublicTransport.Data.Models.Employee employee4 = new PublicTransport.Data.Models.Employee() { EmployeeId = "B0B15B99", DateOfBirth = new DateTime(1999, 07, 02), HireDate = new DateTime(2020, 07, 21), Name = "Johanna Kun", Type = "Maintainer", Wage = 250000 };
                PublicTransport.Data.Models.Employee employee5 = new PublicTransport.Data.Models.Employee() { EmployeeId = "12BFA515", DateOfBirth = new DateTime(1989, 12, 11), HireDate = new DateTime(2018, 03, 12), Name = "Patrik F�ldesi", Type = "Accountant", Wage = 350000 };
                PublicTransport.Data.Models.Employee employee6 = new PublicTransport.Data.Models.Employee() { EmployeeId = "F2837A5C", DateOfBirth = new DateTime(1990, 02, 10), HireDate = new DateTime(2019, 12, 15), Name = "K�roly Kocsis", Type = "Busdriver", Wage = 220000 };
                PublicTransport.Data.Models.Employee employee7 = new PublicTransport.Data.Models.Employee() { EmployeeId = "12D35E89", DateOfBirth = new DateTime(1999, 09, 30), HireDate = new DateTime(2019, 11, 02), Name = "Henrietta Kov�cs", Type = "Trolleydriver", Wage = 320000 };
                pTDB.Add(employee1);
                pTDB.Add(employee2);
                pTDB.Add(employee3);
                pTDB.Add(employee4);
                pTDB.Add(employee5);
                pTDB.Add(employee6);
                pTDB.Add(employee7);
                pTDB.SaveChanges();
            }
        }
    }
}
