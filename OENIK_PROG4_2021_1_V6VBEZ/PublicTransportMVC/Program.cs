namespace PublicTransport.MVC
{
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Hosting;

    /// <summary>
    /// Represents the initalization of the MVC program.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The start point of the application.
        /// </summary>
        /// <param name="args">The parameters at starting. </param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Startup the application.
        /// </summary>
        /// <param name="args">The parameters form main method.</param>
        /// <returns>An IHostBuilder implementation. </returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
