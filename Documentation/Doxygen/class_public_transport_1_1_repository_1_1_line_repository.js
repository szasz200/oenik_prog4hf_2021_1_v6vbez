var class_public_transport_1_1_repository_1_1_line_repository =
[
    [ "LineRepository", "class_public_transport_1_1_repository_1_1_line_repository.html#a2621460d911fd8a145e5883bdb3be63c", null ],
    [ "UpdateLength", "class_public_transport_1_1_repository_1_1_line_repository.html#a9acf3f8a7b8f8010604ca9fd7a24d67e", null ],
    [ "UpdateNumberOfStops", "class_public_transport_1_1_repository_1_1_line_repository.html#a72e904f6586c8706c3d3eea87f7e6889", null ],
    [ "UpdateNumberOfVehicles", "class_public_transport_1_1_repository_1_1_line_repository.html#a2f341a588f05f05b633a3dbce104fd6b", null ],
    [ "UpdateTimeRequired", "class_public_transport_1_1_repository_1_1_line_repository.html#a347cfd272ab9d565293a4ad8e319a9c9", null ],
    [ "UpdateType", "class_public_transport_1_1_repository_1_1_line_repository.html#a3e500c1b163b6afeef866af50aad6309", null ]
];