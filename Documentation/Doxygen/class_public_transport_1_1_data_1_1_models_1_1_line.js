var class_public_transport_1_1_data_1_1_models_1_1_line =
[
    [ "Line", "class_public_transport_1_1_data_1_1_models_1_1_line.html#a05532fd816716659b6b9bdbcc55f6635", null ],
    [ "ToString", "class_public_transport_1_1_data_1_1_models_1_1_line.html#ad82f4f89027053f93ff8c918100fedf2", null ],
    [ "Length", "class_public_transport_1_1_data_1_1_models_1_1_line.html#a16a70ff7933381eae9a62b506fc4b78a", null ],
    [ "LineId", "class_public_transport_1_1_data_1_1_models_1_1_line.html#aad28086a641145d67b0315979bbe8806", null ],
    [ "NumberOfStops", "class_public_transport_1_1_data_1_1_models_1_1_line.html#afa71bcd28c5ca921c28d0a6f8a6bde27", null ],
    [ "NumberOfVehicles", "class_public_transport_1_1_data_1_1_models_1_1_line.html#a4d25f37c8f41eb7b3051460e1e526014", null ],
    [ "TimeRequired", "class_public_transport_1_1_data_1_1_models_1_1_line.html#a9f03fcf33fd5d120ec1595a3306e4edf", null ],
    [ "Type", "class_public_transport_1_1_data_1_1_models_1_1_line.html#a16796a3f472fd8fdedbbb7c263c0109c", null ],
    [ "WorkingDates", "class_public_transport_1_1_data_1_1_models_1_1_line.html#af780825ded98752770d787c31a4da76b", null ]
];