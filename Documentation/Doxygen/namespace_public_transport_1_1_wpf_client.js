var namespace_public_transport_1_1_wpf_client =
[
    [ "App", "class_public_transport_1_1_wpf_client_1_1_app.html", "class_public_transport_1_1_wpf_client_1_1_app" ],
    [ "EditorWindow", "class_public_transport_1_1_wpf_client_1_1_editor_window.html", "class_public_transport_1_1_wpf_client_1_1_editor_window" ],
    [ "EmployeeVM", "class_public_transport_1_1_wpf_client_1_1_employee_v_m.html", "class_public_transport_1_1_wpf_client_1_1_employee_v_m" ],
    [ "IMainLogic", "interface_public_transport_1_1_wpf_client_1_1_i_main_logic.html", "interface_public_transport_1_1_wpf_client_1_1_i_main_logic" ],
    [ "MainLogic", "class_public_transport_1_1_wpf_client_1_1_main_logic.html", "class_public_transport_1_1_wpf_client_1_1_main_logic" ],
    [ "MainVM", "class_public_transport_1_1_wpf_client_1_1_main_v_m.html", "class_public_transport_1_1_wpf_client_1_1_main_v_m" ],
    [ "MainWindow", "class_public_transport_1_1_wpf_client_1_1_main_window.html", "class_public_transport_1_1_wpf_client_1_1_main_window" ],
    [ "MyIoc", "class_public_transport_1_1_wpf_client_1_1_my_ioc.html", "class_public_transport_1_1_wpf_client_1_1_my_ioc" ]
];