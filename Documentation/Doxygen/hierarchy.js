var hierarchy =
[
    [ "PublicTransport.Logic.Test.AccountatLogicTest", "class_public_transport_1_1_logic_1_1_test_1_1_accountat_logic_test.html", null ],
    [ "PublicTransport.MVC.Controllers.ApiResult", "class_public_transport_1_1_m_v_c_1_1_controllers_1_1_api_result.html", null ],
    [ "Application", null, [
      [ "PublicTransport.App", "class_public_transport_1_1_app.html", null ],
      [ "PublicTransport.WpfClient.App", "class_public_transport_1_1_wpf_client_1_1_app.html", null ]
    ] ],
    [ "Controller", null, [
      [ "PublicTransport.MVC.Controllers.EmployeesController", "class_public_transport_1_1_m_v_c_1_1_controllers_1_1_employees_controller.html", null ],
      [ "PublicTransport.MVC.Controllers.HomeController", "class_public_transport_1_1_m_v_c_1_1_controllers_1_1_home_controller.html", null ],
      [ "PublicTransport.MVC.Controllers.PublicTransportAPIController", "class_public_transport_1_1_m_v_c_1_1_controllers_1_1_public_transport_a_p_i_controller.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "PublicTransport.Data.Models.PTDBContext", "class_public_transport_1_1_data_1_1_models_1_1_p_t_d_b_context.html", null ]
    ] ],
    [ "PublicTransport.MVC.Models.Employee", "class_public_transport_1_1_m_v_c_1_1_models_1_1_employee.html", null ],
    [ "PublicTransport.Data.Models.Employee", "class_public_transport_1_1_data_1_1_models_1_1_employee.html", null ],
    [ "PublicTransport.MVC.Models.EmployeeListViewModel", "class_public_transport_1_1_m_v_c_1_1_models_1_1_employee_list_view_model.html", null ],
    [ "PublicTransport.MVC.Models.ErrorViewModel", "class_public_transport_1_1_m_v_c_1_1_models_1_1_error_view_model.html", null ],
    [ "PublicTransportWPF.Data.Factory", "class_public_transport_w_p_f_1_1_data_1_1_factory.html", null ],
    [ "PublicTransport.Logic.Test.HrLogicTests", "class_public_transport_1_1_logic_1_1_test_1_1_hr_logic_tests.html", null ],
    [ "PublicTransport.Logic.IAccountantLogic", "interface_public_transport_1_1_logic_1_1_i_accountant_logic.html", [
      [ "PublicTransport.Logic.AccountantLogic", "class_public_transport_1_1_logic_1_1_accountant_logic.html", null ]
    ] ],
    [ "IComponentConnector", null, [
      [ "PublicTransportWPF.UI.EmployeeSelectorWindows", "class_public_transport_w_p_f_1_1_u_i_1_1_employee_selector_windows.html", null ],
      [ "PublicTransportWPF.UI.EmpolyeeEditor", "class_public_transport_w_p_f_1_1_u_i_1_1_empolyee_editor.html", null ]
    ] ],
    [ "PublicTransportWPF.BL.IEditorService", "interface_public_transport_w_p_f_1_1_b_l_1_1_i_editor_service.html", [
      [ "PublicTransportWPF.UI.EmployeeEditorServiceViaWindows", "class_public_transport_w_p_f_1_1_u_i_1_1_employee_editor_service_via_windows.html", null ]
    ] ],
    [ "PublicTransportWPF.BL.IEntityLogic", "interface_public_transport_w_p_f_1_1_b_l_1_1_i_entity_logic.html", [
      [ "PublicTransportWPF.BL.EntityLogic< T >", "class_public_transport_w_p_f_1_1_b_l_1_1_entity_logic.html", null ]
    ] ],
    [ "IEquatable", null, [
      [ "PublicTransport.Logic.TypeAvarageLength", "class_public_transport_1_1_logic_1_1_type_avarage_length.html", null ]
    ] ],
    [ "PublicTransport.Logic.IHumanResourceLogic", "interface_public_transport_1_1_logic_1_1_i_human_resource_logic.html", [
      [ "PublicTransport.Logic.HumanResourceLogic", "class_public_transport_1_1_logic_1_1_human_resource_logic.html", null ]
    ] ],
    [ "PublicTransport.WpfClient.IMainLogic", "interface_public_transport_1_1_wpf_client_1_1_i_main_logic.html", [
      [ "PublicTransport.WpfClient.MainLogic", "class_public_transport_1_1_wpf_client_1_1_main_logic.html", null ]
    ] ],
    [ "PublicTransport.Logic.INetworkManagementLogic", "interface_public_transport_1_1_logic_1_1_i_network_management_logic.html", [
      [ "PublicTransport.Logic.NetworkManagementLogic", "class_public_transport_1_1_logic_1_1_network_management_logic.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "PublicTransportWPF.Data.IPublicTransportEntity", "interface_public_transport_w_p_f_1_1_data_1_1_i_public_transport_entity.html", [
      [ "PublicTransportWPF.Data.Employee", "class_public_transport_w_p_f_1_1_data_1_1_employee.html", null ],
      [ "PublicTransportWPF.Data.Line", "class_public_transport_w_p_f_1_1_data_1_1_line.html", null ],
      [ "PublicTransportWPF.Data.Maintenance", "class_public_transport_w_p_f_1_1_data_1_1_maintenance.html", null ],
      [ "PublicTransportWPF.Data.Vehicle", "class_public_transport_w_p_f_1_1_data_1_1_vehicle.html", null ],
      [ "PublicTransportWPF.Data.WorkingDate", "class_public_transport_w_p_f_1_1_data_1_1_working_date.html", null ]
    ] ],
    [ "PublicTransport.Repository.IRepository< T >", "interface_public_transport_1_1_repository_1_1_i_repository.html", [
      [ "PublicTransport.Repository.Repositories< T >", "class_public_transport_1_1_repository_1_1_repositories.html", null ]
    ] ],
    [ "PublicTransport.Repository.IRepository< Employee >", "interface_public_transport_1_1_repository_1_1_i_repository.html", [
      [ "PublicTransport.Repository.IEmployeeRepository", "interface_public_transport_1_1_repository_1_1_i_employee_repository.html", [
        [ "PublicTransport.Repository.EmployeeRepository", "class_public_transport_1_1_repository_1_1_employee_repository.html", null ]
      ] ]
    ] ],
    [ "PublicTransport.Repository.IRepository< Line >", "interface_public_transport_1_1_repository_1_1_i_repository.html", [
      [ "PublicTransport.Repository.ILineRepository", "interface_public_transport_1_1_repository_1_1_i_line_repository.html", [
        [ "PublicTransport.Repository.LineRepository", "class_public_transport_1_1_repository_1_1_line_repository.html", null ]
      ] ]
    ] ],
    [ "PublicTransport.Repository.IRepository< Maintenance >", "interface_public_transport_1_1_repository_1_1_i_repository.html", [
      [ "PublicTransport.Repository.IMaintenanceRepository", "interface_public_transport_1_1_repository_1_1_i_maintenance_repository.html", [
        [ "PublicTransport.Repository.MaintenanceRepository", "class_public_transport_1_1_repository_1_1_maintenance_repository.html", null ]
      ] ]
    ] ],
    [ "PublicTransport.Repository.IRepository< Vehicle >", "interface_public_transport_1_1_repository_1_1_i_repository.html", [
      [ "PublicTransport.Repository.IVehicleRepository", "interface_public_transport_1_1_repository_1_1_i_vehicle_repository.html", [
        [ "PublicTransport.Repository.VehicleRepository", "class_public_transport_1_1_repository_1_1_vehicle_repository.html", null ]
      ] ]
    ] ],
    [ "PublicTransport.Repository.IRepository< WorkingDate >", "interface_public_transport_1_1_repository_1_1_i_repository.html", [
      [ "PublicTransport.Repository.IWorkingDateRepository", "interface_public_transport_1_1_repository_1_1_i_working_date_repository.html", [
        [ "PublicTransport.Repository.WorkingDateRepository", "class_public_transport_1_1_repository_1_1_working_date_repository.html", null ]
      ] ]
    ] ],
    [ "IServiceLocator", null, [
      [ "PublicTransport.MyIoc", "class_public_transport_1_1_my_ioc.html", null ],
      [ "PublicTransport.WpfClient.MyIoc", "class_public_transport_1_1_wpf_client_1_1_my_ioc.html", null ]
    ] ],
    [ "PublicTransport.Logic.IVehicleManagementLogic", "interface_public_transport_1_1_logic_1_1_i_vehicle_management_logic.html", [
      [ "PublicTransport.Logic.VehicleManagementLogic", "class_public_transport_1_1_logic_1_1_vehicle_management_logic.html", null ]
    ] ],
    [ "PublicTransport.Data.Models.Line", "class_public_transport_1_1_data_1_1_models_1_1_line.html", null ],
    [ "PublicTransport.Data.Models.Maintenance", "class_public_transport_1_1_data_1_1_models_1_1_maintenance.html", null ],
    [ "PublicTransport.MVC.Models.MapperFactory", "class_public_transport_1_1_m_v_c_1_1_models_1_1_mapper_factory.html", null ],
    [ "PublicTransport.Logic.Test.NetworkManangementLogicTest", "class_public_transport_1_1_logic_1_1_test_1_1_network_manangement_logic_test.html", null ],
    [ "ObservableObject", null, [
      [ "PublicTransport.WpfClient.EmployeeVM", "class_public_transport_1_1_wpf_client_1_1_employee_v_m.html", null ],
      [ "PublicTransportWPF.Data.Employee", "class_public_transport_w_p_f_1_1_data_1_1_employee.html", null ],
      [ "PublicTransportWPF.Data.Line", "class_public_transport_w_p_f_1_1_data_1_1_line.html", null ],
      [ "PublicTransportWPF.Data.Maintenance", "class_public_transport_w_p_f_1_1_data_1_1_maintenance.html", null ],
      [ "PublicTransportWPF.Data.Vehicle", "class_public_transport_w_p_f_1_1_data_1_1_vehicle.html", null ],
      [ "PublicTransportWPF.Data.WorkingDate", "class_public_transport_w_p_f_1_1_data_1_1_working_date.html", null ]
    ] ],
    [ "PublicTransport.MVC.Program", "class_public_transport_1_1_m_v_c_1_1_program.html", null ],
    [ "RazorPage", null, [
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ],
      [ "AspNetCore.Views_Employees_EmployeesDetails", "class_asp_net_core_1_1_views___employees___employees_details.html", null ],
      [ "AspNetCore.Views_Employees_EmployeesEdit", "class_asp_net_core_1_1_views___employees___employees_edit.html", null ],
      [ "AspNetCore.Views_Employees_EmployeesIndex", "class_asp_net_core_1_1_views___employees___employees_index.html", null ],
      [ "AspNetCore.Views_Employees_EmployeesNew", "class_asp_net_core_1_1_views___employees___employees_new.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ]
    ] ],
    [ "RazorPage< IEnumerable< PublicTransport.MVC.Models.Employee >>", null, [
      [ "AspNetCore.Views_Employees_EmployeesList", "class_asp_net_core_1_1_views___employees___employees_list.html", null ]
    ] ],
    [ "PublicTransport.Repository.Repositories< Employee >", "class_public_transport_1_1_repository_1_1_repositories.html", [
      [ "PublicTransport.Repository.EmployeeRepository", "class_public_transport_1_1_repository_1_1_employee_repository.html", null ]
    ] ],
    [ "PublicTransport.Repository.Repositories< Line >", "class_public_transport_1_1_repository_1_1_repositories.html", [
      [ "PublicTransport.Repository.LineRepository", "class_public_transport_1_1_repository_1_1_line_repository.html", null ]
    ] ],
    [ "PublicTransport.Repository.Repositories< Maintenance >", "class_public_transport_1_1_repository_1_1_repositories.html", [
      [ "PublicTransport.Repository.MaintenanceRepository", "class_public_transport_1_1_repository_1_1_maintenance_repository.html", null ]
    ] ],
    [ "PublicTransport.Repository.Repositories< Vehicle >", "class_public_transport_1_1_repository_1_1_repositories.html", [
      [ "PublicTransport.Repository.VehicleRepository", "class_public_transport_1_1_repository_1_1_vehicle_repository.html", null ]
    ] ],
    [ "PublicTransport.Repository.Repositories< WorkingDate >", "class_public_transport_1_1_repository_1_1_repositories.html", [
      [ "PublicTransport.Repository.WorkingDateRepository", "class_public_transport_1_1_repository_1_1_working_date_repository.html", null ]
    ] ],
    [ "SimpleIoc", null, [
      [ "PublicTransport.MyIoc", "class_public_transport_1_1_my_ioc.html", null ],
      [ "PublicTransport.WpfClient.MyIoc", "class_public_transport_1_1_wpf_client_1_1_my_ioc.html", null ]
    ] ],
    [ "PublicTransport.MVC.Startup", "class_public_transport_1_1_m_v_c_1_1_startup.html", null ],
    [ "PublicTransport.Logic.TypeAvarageA", "class_public_transport_1_1_logic_1_1_type_avarage_a.html", null ],
    [ "PublicTransport.Logic.TypeAvarageTD", "class_public_transport_1_1_logic_1_1_type_avarage_t_d.html", null ],
    [ "PublicTransport.Logic.TypeCount", "class_public_transport_1_1_logic_1_1_type_count.html", null ],
    [ "PublicTransport.Data.Models.Vehicle", "class_public_transport_1_1_data_1_1_models_1_1_vehicle.html", null ],
    [ "PublicTransport.Logic.VehicleCost", "class_public_transport_1_1_logic_1_1_vehicle_cost.html", null ],
    [ "PublicTransport.Logic.Test.VehicleManangementLogicTest", "class_public_transport_1_1_logic_1_1_test_1_1_vehicle_manangement_logic_test.html", null ],
    [ "PublicTransport.Logic.VehicleNextMaintances", "class_public_transport_1_1_logic_1_1_vehicle_next_maintances.html", null ],
    [ "ViewModelBase", null, [
      [ "PublicTransport.WpfClient.MainVM", "class_public_transport_1_1_wpf_client_1_1_main_v_m.html", null ],
      [ "PublicTransportWPF.VM.EmployeeEditorViewModel", "class_public_transport_w_p_f_1_1_v_m_1_1_employee_editor_view_model.html", null ],
      [ "PublicTransportWPF.VM.EmpolyeeSelectorViewModel", "class_public_transport_w_p_f_1_1_v_m_1_1_empolyee_selector_view_model.html", null ],
      [ "PublicTransportWPF.VM.MainViewModel", "class_public_transport_w_p_f_1_1_v_m_1_1_main_view_model.html", null ]
    ] ],
    [ "Window", null, [
      [ "PublicTransport.MainWindow", "class_public_transport_1_1_main_window.html", null ],
      [ "PublicTransport.WpfClient.EditorWindow", "class_public_transport_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "PublicTransport.WpfClient.MainWindow", "class_public_transport_1_1_wpf_client_1_1_main_window.html", null ],
      [ "PublicTransportWPF.UI.EmployeeSelectorWindows", "class_public_transport_w_p_f_1_1_u_i_1_1_employee_selector_windows.html", null ],
      [ "PublicTransportWPF.UI.EmpolyeeEditor", "class_public_transport_w_p_f_1_1_u_i_1_1_empolyee_editor.html", null ]
    ] ],
    [ "PublicTransport.Logic.WorkHours", "class_public_transport_1_1_logic_1_1_work_hours.html", null ],
    [ "PublicTransport.Data.Models.WorkingDate", "class_public_transport_1_1_data_1_1_models_1_1_working_date.html", null ]
];