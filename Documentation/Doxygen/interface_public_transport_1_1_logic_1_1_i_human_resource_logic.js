var interface_public_transport_1_1_logic_1_1_i_human_resource_logic =
[
    [ "ChangeName", "interface_public_transport_1_1_logic_1_1_i_human_resource_logic.html#a33f310ebb8ad79bf65ac17d84312a2ca", null ],
    [ "ChangePositon", "interface_public_transport_1_1_logic_1_1_i_human_resource_logic.html#a2fae7372921f086071af6a363a32d366", null ],
    [ "ChangeWage", "interface_public_transport_1_1_logic_1_1_i_human_resource_logic.html#a578f8b7db03f65002056c6e8009f009f", null ],
    [ "GetAllEmployee", "interface_public_transport_1_1_logic_1_1_i_human_resource_logic.html#a92e912bc0ed2268337185e1670cd9cc4", null ],
    [ "GetBeginers", "interface_public_transport_1_1_logic_1_1_i_human_resource_logic.html#aa11af7dbe5b2581bf81535b1e5cdaccb", null ],
    [ "GetBirthdays", "interface_public_transport_1_1_logic_1_1_i_human_resource_logic.html#a59e5124a23fad243f027d56d6557fe72", null ],
    [ "GetById", "interface_public_transport_1_1_logic_1_1_i_human_resource_logic.html#afa24acd1775b470c4b4d37512058f421", null ],
    [ "GetByName", "interface_public_transport_1_1_logic_1_1_i_human_resource_logic.html#ad88f9c41742d98947416d23671aea2d8", null ],
    [ "GetByPosition", "interface_public_transport_1_1_logic_1_1_i_human_resource_logic.html#ab32d8b0534823f97dbe2506b4823dbc8", null ],
    [ "NewEmployee", "interface_public_transport_1_1_logic_1_1_i_human_resource_logic.html#a6e9f71a42f74e094b264a50495a4ab36", null ],
    [ "RemoveEmployee", "interface_public_transport_1_1_logic_1_1_i_human_resource_logic.html#a4279f4e60111425a97b110cd1cf6df66", null ]
];