var class_public_transport_w_p_f_1_1_data_1_1_vehicle =
[
    [ "AddMethod", "class_public_transport_w_p_f_1_1_data_1_1_vehicle.html#abff9a15fdb58e3205808d725b727c22e", null ],
    [ "CopyFrom", "class_public_transport_w_p_f_1_1_data_1_1_vehicle.html#a708c6ad17ecdd8dc027a5375184365fa", null ],
    [ "DeleteMethod", "class_public_transport_w_p_f_1_1_data_1_1_vehicle.html#a8f811615a5a9524fde2d370e9e9269ae", null ],
    [ "GetAllMethod", "class_public_transport_w_p_f_1_1_data_1_1_vehicle.html#a9ef66bd6470f4c88993ddb5bf1d8761f", null ],
    [ "ModifyMethod", "class_public_transport_w_p_f_1_1_data_1_1_vehicle.html#af26dae44ec76c44930990854e9cfe4f9", null ],
    [ "ToString", "class_public_transport_w_p_f_1_1_data_1_1_vehicle.html#a44e848d4faf6f39c10bdfdce58c7d61f", null ],
    [ "LastMaintenanceId", "class_public_transport_w_p_f_1_1_data_1_1_vehicle.html#a8f71d58ec4736d7bfab7fa52c9e3a1c6", null ],
    [ "Lpnumber", "class_public_transport_w_p_f_1_1_data_1_1_vehicle.html#a9d28e00aef076b25e3d45c4424940749", null ],
    [ "NextMaintenance", "class_public_transport_w_p_f_1_1_data_1_1_vehicle.html#a2c8a28c6c6633b67a88eb416de4d0fe2", null ],
    [ "TraveledDistance", "class_public_transport_w_p_f_1_1_data_1_1_vehicle.html#ab3edcb729d13782d0d7b9c96bddd8bdc", null ],
    [ "Type", "class_public_transport_w_p_f_1_1_data_1_1_vehicle.html#af3f714fff71b5ab546264cdcc926d04a", null ],
    [ "YearOfManufacture", "class_public_transport_w_p_f_1_1_data_1_1_vehicle.html#a5810c7c51ee6da55c917526feb2f4129", null ]
];