var class_public_transport_1_1_repository_1_1_vehicle_repository =
[
    [ "VehicleRepository", "class_public_transport_1_1_repository_1_1_vehicle_repository.html#aef6180b3a67539c6e51ad82a32254948", null ],
    [ "UpdateLastMaintenanceId", "class_public_transport_1_1_repository_1_1_vehicle_repository.html#ad7300ef98e46a9ec2c811971dcdea4f2", null ],
    [ "UpdateNextMaintenance", "class_public_transport_1_1_repository_1_1_vehicle_repository.html#a9649a827907035e9bba06a7a2faca34a", null ],
    [ "UpdateTraveledDistance", "class_public_transport_1_1_repository_1_1_vehicle_repository.html#a3e6ffc9cabf723386b9f83630af6d7d2", null ]
];