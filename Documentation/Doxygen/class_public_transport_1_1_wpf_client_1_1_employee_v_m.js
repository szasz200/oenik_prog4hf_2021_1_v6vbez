var class_public_transport_1_1_wpf_client_1_1_employee_v_m =
[
    [ "CopyFrom", "class_public_transport_1_1_wpf_client_1_1_employee_v_m.html#a837ce1fedab2c9d71f5a566dcaf53bcb", null ],
    [ "ToString", "class_public_transport_1_1_wpf_client_1_1_employee_v_m.html#a00d732a30d5fc04c30cfe124e74081a0", null ],
    [ "DateOfBirth", "class_public_transport_1_1_wpf_client_1_1_employee_v_m.html#a911171b4fcf20bf1f4e9f3b3cce3c563", null ],
    [ "EmployeeId", "class_public_transport_1_1_wpf_client_1_1_employee_v_m.html#ae33a4e1a42a7d8045e1fe00e3e97e4ea", null ],
    [ "HireDate", "class_public_transport_1_1_wpf_client_1_1_employee_v_m.html#a5a15f9878cd7300d530ecefcc3ebcfbe", null ],
    [ "IsAdd", "class_public_transport_1_1_wpf_client_1_1_employee_v_m.html#acdd8ee06ed1188a1ba424695ed433869", null ],
    [ "JobTypes", "class_public_transport_1_1_wpf_client_1_1_employee_v_m.html#a8dda77e5f30d92ab34f46488980d1f76", null ],
    [ "Name", "class_public_transport_1_1_wpf_client_1_1_employee_v_m.html#aacf5fa61fea574c48641a715e0bc13d3", null ],
    [ "Type", "class_public_transport_1_1_wpf_client_1_1_employee_v_m.html#a22307c2a74f88040bda2da4596022de8", null ],
    [ "Wage", "class_public_transport_1_1_wpf_client_1_1_employee_v_m.html#afa426f78675e1a9c4d8d074814ce4ee7", null ]
];