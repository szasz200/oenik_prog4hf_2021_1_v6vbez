var class_public_transport_1_1_m_v_c_1_1_controllers_1_1_public_transport_a_p_i_controller =
[
    [ "PublicTransportAPIController", "class_public_transport_1_1_m_v_c_1_1_controllers_1_1_public_transport_a_p_i_controller.html#a4f3cbf3e2b2e757ffb13766c0416810e", null ],
    [ "AddOneEmployee", "class_public_transport_1_1_m_v_c_1_1_controllers_1_1_public_transport_a_p_i_controller.html#a915e2dcdc578893464bc1845b7654ec8", null ],
    [ "DelOneEmployee", "class_public_transport_1_1_m_v_c_1_1_controllers_1_1_public_transport_a_p_i_controller.html#ab9b6180524f54c89a05c1f95a7602a4a", null ],
    [ "GetAll", "class_public_transport_1_1_m_v_c_1_1_controllers_1_1_public_transport_a_p_i_controller.html#a4b1ad6e5848b54063157066903b733a9", null ],
    [ "ModOneEmployee", "class_public_transport_1_1_m_v_c_1_1_controllers_1_1_public_transport_a_p_i_controller.html#a40a72d57deee78ec349fd90d0cc82ff1", null ]
];