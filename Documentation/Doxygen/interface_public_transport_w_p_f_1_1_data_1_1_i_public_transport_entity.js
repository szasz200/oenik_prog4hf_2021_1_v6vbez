var interface_public_transport_w_p_f_1_1_data_1_1_i_public_transport_entity =
[
    [ "AddMethod", "interface_public_transport_w_p_f_1_1_data_1_1_i_public_transport_entity.html#adb2ef326c6c2b1374476f9f24c497938", null ],
    [ "CopyFrom", "interface_public_transport_w_p_f_1_1_data_1_1_i_public_transport_entity.html#a096279a7299bbd2d1f2a4592fcf5494f", null ],
    [ "DeleteMethod", "interface_public_transport_w_p_f_1_1_data_1_1_i_public_transport_entity.html#a17dcb495ed0c1e4e63e44a8fa491b884", null ],
    [ "GetAllMethod", "interface_public_transport_w_p_f_1_1_data_1_1_i_public_transport_entity.html#ad9d1df7e527c230e46c0b261d490f25d", null ],
    [ "ModifyMethod", "interface_public_transport_w_p_f_1_1_data_1_1_i_public_transport_entity.html#a4056dd2d1feff8f59bcd188ac8b70f6f", null ]
];