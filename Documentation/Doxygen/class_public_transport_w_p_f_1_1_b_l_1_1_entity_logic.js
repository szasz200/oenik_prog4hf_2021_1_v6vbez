var class_public_transport_w_p_f_1_1_b_l_1_1_entity_logic =
[
    [ "EntityLogic", "class_public_transport_w_p_f_1_1_b_l_1_1_entity_logic.html#a0411e861d21409a02362b7b723626776", null ],
    [ "AddEntity", "class_public_transport_w_p_f_1_1_b_l_1_1_entity_logic.html#a37e97b3226f67ef072e237db701752f9", null ],
    [ "DeleteEntity", "class_public_transport_w_p_f_1_1_b_l_1_1_entity_logic.html#a1292875d1ec1e6a6e053b7e4241c47b0", null ],
    [ "GetAllEntity", "class_public_transport_w_p_f_1_1_b_l_1_1_entity_logic.html#aef3395d451e2223f179f306ff6d14fa4", null ],
    [ "ModifyEntity", "class_public_transport_w_p_f_1_1_b_l_1_1_entity_logic.html#a150fd1f88717e15d9a0eaee70a70e8c3", null ]
];