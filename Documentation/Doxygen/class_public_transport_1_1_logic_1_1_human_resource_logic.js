var class_public_transport_1_1_logic_1_1_human_resource_logic =
[
    [ "HumanResourceLogic", "class_public_transport_1_1_logic_1_1_human_resource_logic.html#a53c9d7d6bb760f2c528143ae8ab91c85", null ],
    [ "ChangeName", "class_public_transport_1_1_logic_1_1_human_resource_logic.html#a3023ac7798ffe38ca17001e31d8afe19", null ],
    [ "ChangePositon", "class_public_transport_1_1_logic_1_1_human_resource_logic.html#a751bf8fb873ce93dd7d11045989b26c2", null ],
    [ "ChangeWage", "class_public_transport_1_1_logic_1_1_human_resource_logic.html#a5e99803d5a2e9e156176db0d970cdee8", null ],
    [ "GetAllEmployee", "class_public_transport_1_1_logic_1_1_human_resource_logic.html#a22fc36cc14bf4764c42bdf82d57ef79f", null ],
    [ "GetBeginers", "class_public_transport_1_1_logic_1_1_human_resource_logic.html#a772ad17b8ce467149780389e05aa89bf", null ],
    [ "GetBirthdays", "class_public_transport_1_1_logic_1_1_human_resource_logic.html#a05bfb7c569b4f40da597c4705c9798e8", null ],
    [ "GetById", "class_public_transport_1_1_logic_1_1_human_resource_logic.html#abf76b960d1f3d67b6b16889048c86a9a", null ],
    [ "GetByName", "class_public_transport_1_1_logic_1_1_human_resource_logic.html#a2915ff3e2655df9a9de638e39b873da8", null ],
    [ "GetByPosition", "class_public_transport_1_1_logic_1_1_human_resource_logic.html#a7d05189558a9fdeb228df1f00fd09248", null ],
    [ "NewEmployee", "class_public_transport_1_1_logic_1_1_human_resource_logic.html#a733e19b8af1ba8717aabec88deeab636", null ],
    [ "RemoveEmployee", "class_public_transport_1_1_logic_1_1_human_resource_logic.html#aa95b1e037eb9d215361635246a9c37fe", null ]
];