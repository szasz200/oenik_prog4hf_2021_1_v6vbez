var interface_public_transport_1_1_logic_1_1_i_vehicle_management_logic =
[
    [ "ChangeNextMaintenance", "interface_public_transport_1_1_logic_1_1_i_vehicle_management_logic.html#a83907135c89719d484d506e0dbb4489e", null ],
    [ "GetAvarageTypeTDistances", "interface_public_transport_1_1_logic_1_1_i_vehicle_management_logic.html#aeaef9eec0c21a5487a44691455fffacd", null ],
    [ "GetAvarageVehicleTypeAge", "interface_public_transport_1_1_logic_1_1_i_vehicle_management_logic.html#a513290904b6a045c04822c1de7f57c50", null ],
    [ "GetMaintenances", "interface_public_transport_1_1_logic_1_1_i_vehicle_management_logic.html#ab38a7a7dbfdad7138db6b7d689012b26", null ],
    [ "GetNextMaintances", "interface_public_transport_1_1_logic_1_1_i_vehicle_management_logic.html#a8285806a7279e752c58b30f2666599d5", null ],
    [ "GetTodayMaintenances", "interface_public_transport_1_1_logic_1_1_i_vehicle_management_logic.html#a472776420fddd3a0cc8de718d377d942", null ],
    [ "GetVehicle", "interface_public_transport_1_1_logic_1_1_i_vehicle_management_logic.html#acf40e08bc372b2ccd580855e7d30db67", null ],
    [ "NewMaintenance", "interface_public_transport_1_1_logic_1_1_i_vehicle_management_logic.html#a89e1beff370630a06e1f45bcd7a4508c", null ],
    [ "NewVehicle", "interface_public_transport_1_1_logic_1_1_i_vehicle_management_logic.html#a47dbc6341fe95b1d7c04f4f4d51fd155", null ],
    [ "RemoveVehicle", "interface_public_transport_1_1_logic_1_1_i_vehicle_management_logic.html#a4eadc41d31d78647e508b398de5fa7ce", null ],
    [ "Vehicles", "interface_public_transport_1_1_logic_1_1_i_vehicle_management_logic.html#a7086833068d7e47dd5590ae77b047f08", null ]
];