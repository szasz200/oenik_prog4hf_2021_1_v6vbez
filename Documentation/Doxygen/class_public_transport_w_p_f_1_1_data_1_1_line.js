var class_public_transport_w_p_f_1_1_data_1_1_line =
[
    [ "AddMethod", "class_public_transport_w_p_f_1_1_data_1_1_line.html#a17fdf1bde7009c7fe8da3b110134254a", null ],
    [ "CopyFrom", "class_public_transport_w_p_f_1_1_data_1_1_line.html#ad4cc281c721ff498e22ad170ad42b49e", null ],
    [ "DeleteMethod", "class_public_transport_w_p_f_1_1_data_1_1_line.html#a277ac7317dc3b4deb33c27017a531ab2", null ],
    [ "GetAllMethod", "class_public_transport_w_p_f_1_1_data_1_1_line.html#a6f8e2080959574270687d95fc77f3176", null ],
    [ "ModifyMethod", "class_public_transport_w_p_f_1_1_data_1_1_line.html#a5722c149e5ab0b6354f854d876870179", null ],
    [ "ToString", "class_public_transport_w_p_f_1_1_data_1_1_line.html#a2949a8354892490f765ddcbf07250fff", null ],
    [ "Length", "class_public_transport_w_p_f_1_1_data_1_1_line.html#adbf1d449a9851aa975aa3a97e131eebb", null ],
    [ "LineId", "class_public_transport_w_p_f_1_1_data_1_1_line.html#a2a4e4b75b704a5c73be8523cf620dff2", null ],
    [ "NumberOfStops", "class_public_transport_w_p_f_1_1_data_1_1_line.html#a7b2793e98a5e0fc00ade50d438e48da4", null ],
    [ "NumberOfVehicles", "class_public_transport_w_p_f_1_1_data_1_1_line.html#ab8d49efbe1192212806d5b2e408a2707", null ],
    [ "TimeRequired", "class_public_transport_w_p_f_1_1_data_1_1_line.html#ae5d55645487b066cd471bb6e586f62e3", null ],
    [ "Type", "class_public_transport_w_p_f_1_1_data_1_1_line.html#a342d45908464caa1e5030a01f6effe9b", null ]
];