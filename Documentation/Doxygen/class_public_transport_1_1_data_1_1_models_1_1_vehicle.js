var class_public_transport_1_1_data_1_1_models_1_1_vehicle =
[
    [ "Vehicle", "class_public_transport_1_1_data_1_1_models_1_1_vehicle.html#a48e0404b26f850efe71d2ab96765e150", null ],
    [ "ToString", "class_public_transport_1_1_data_1_1_models_1_1_vehicle.html#a68dc2cbd8a70a1fa3ba60c8aca0a67bd", null ],
    [ "LastMaintenance", "class_public_transport_1_1_data_1_1_models_1_1_vehicle.html#a04d3f560fe1350242be80ad8025c3f34", null ],
    [ "LastMaintenanceId", "class_public_transport_1_1_data_1_1_models_1_1_vehicle.html#a68ebb8d0bd1a5cb2f4a6ed3281ed3e5a", null ],
    [ "Lpnumber", "class_public_transport_1_1_data_1_1_models_1_1_vehicle.html#a1357c1099590290bd79a6b0b45bae959", null ],
    [ "Maintenances", "class_public_transport_1_1_data_1_1_models_1_1_vehicle.html#aa415c7b2289fe598e0127d28eba868d7", null ],
    [ "NextMaintenance", "class_public_transport_1_1_data_1_1_models_1_1_vehicle.html#add10aac396e64b5e249f3f6c182153d3", null ],
    [ "TraveledDistance", "class_public_transport_1_1_data_1_1_models_1_1_vehicle.html#a3aa62a6607af6c8d85f12a9f6ee09ce1", null ],
    [ "Type", "class_public_transport_1_1_data_1_1_models_1_1_vehicle.html#a17e66baef1cbedea4ce799966b506301", null ],
    [ "WorkingDates", "class_public_transport_1_1_data_1_1_models_1_1_vehicle.html#a8d18eb573753d66c40baaf0a57f249b2", null ],
    [ "YearOfManufacture", "class_public_transport_1_1_data_1_1_models_1_1_vehicle.html#a9cbf66bbccc60b9f75826e63e57bddfc", null ]
];