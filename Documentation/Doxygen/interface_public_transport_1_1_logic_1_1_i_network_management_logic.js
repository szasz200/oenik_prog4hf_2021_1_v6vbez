var interface_public_transport_1_1_logic_1_1_i_network_management_logic =
[
    [ "AvarageLineTypeLengths", "interface_public_transport_1_1_logic_1_1_i_network_management_logic.html#ae94494a5a17c9c7eb49c92cf80f203cb", null ],
    [ "ChangeLength", "interface_public_transport_1_1_logic_1_1_i_network_management_logic.html#a4ca065c26995221705ad762a3516b8c8", null ],
    [ "ChangeNumberOfStops", "interface_public_transport_1_1_logic_1_1_i_network_management_logic.html#a7bdb68bd31f79ea6b2d60a2d485c7463", null ],
    [ "ChangeNumberOfVehicles", "interface_public_transport_1_1_logic_1_1_i_network_management_logic.html#aaa44b850c36406c1fc9aefc68644b926", null ],
    [ "ChangeTimeRequired", "interface_public_transport_1_1_logic_1_1_i_network_management_logic.html#a99796ff9f3fc6ce441cd0e81583f4822", null ],
    [ "GetLine", "interface_public_transport_1_1_logic_1_1_i_network_management_logic.html#aba4d457b8c5dc59d2e22c4660c1c4677", null ],
    [ "GetLines", "interface_public_transport_1_1_logic_1_1_i_network_management_logic.html#a56e3972e330ac897237455d1ed083ea8", null ],
    [ "GetTypeCounts", "interface_public_transport_1_1_logic_1_1_i_network_management_logic.html#ac191f7826a83a82c15f7f3f850169927", null ],
    [ "LengthOfNetwork", "interface_public_transport_1_1_logic_1_1_i_network_management_logic.html#aba236aae3ef89cd536ea922629463118", null ],
    [ "NewLine", "interface_public_transport_1_1_logic_1_1_i_network_management_logic.html#a5c6253d51fa90da03604c33e014d7f24", null ]
];