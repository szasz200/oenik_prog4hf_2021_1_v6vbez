var namespace_public_transport_1_1_logic =
[
    [ "Test", "namespace_public_transport_1_1_logic_1_1_test.html", "namespace_public_transport_1_1_logic_1_1_test" ],
    [ "AccountantLogic", "class_public_transport_1_1_logic_1_1_accountant_logic.html", "class_public_transport_1_1_logic_1_1_accountant_logic" ],
    [ "HumanResourceLogic", "class_public_transport_1_1_logic_1_1_human_resource_logic.html", "class_public_transport_1_1_logic_1_1_human_resource_logic" ],
    [ "IAccountantLogic", "interface_public_transport_1_1_logic_1_1_i_accountant_logic.html", "interface_public_transport_1_1_logic_1_1_i_accountant_logic" ],
    [ "IHumanResourceLogic", "interface_public_transport_1_1_logic_1_1_i_human_resource_logic.html", "interface_public_transport_1_1_logic_1_1_i_human_resource_logic" ],
    [ "INetworkManagementLogic", "interface_public_transport_1_1_logic_1_1_i_network_management_logic.html", "interface_public_transport_1_1_logic_1_1_i_network_management_logic" ],
    [ "IVehicleManagementLogic", "interface_public_transport_1_1_logic_1_1_i_vehicle_management_logic.html", "interface_public_transport_1_1_logic_1_1_i_vehicle_management_logic" ],
    [ "NetworkManagementLogic", "class_public_transport_1_1_logic_1_1_network_management_logic.html", "class_public_transport_1_1_logic_1_1_network_management_logic" ],
    [ "TypeAvarageA", "class_public_transport_1_1_logic_1_1_type_avarage_a.html", "class_public_transport_1_1_logic_1_1_type_avarage_a" ],
    [ "TypeAvarageLength", "class_public_transport_1_1_logic_1_1_type_avarage_length.html", "class_public_transport_1_1_logic_1_1_type_avarage_length" ],
    [ "TypeAvarageTD", "class_public_transport_1_1_logic_1_1_type_avarage_t_d.html", "class_public_transport_1_1_logic_1_1_type_avarage_t_d" ],
    [ "TypeCount", "class_public_transport_1_1_logic_1_1_type_count.html", "class_public_transport_1_1_logic_1_1_type_count" ],
    [ "VehicleCost", "class_public_transport_1_1_logic_1_1_vehicle_cost.html", "class_public_transport_1_1_logic_1_1_vehicle_cost" ],
    [ "VehicleManagementLogic", "class_public_transport_1_1_logic_1_1_vehicle_management_logic.html", "class_public_transport_1_1_logic_1_1_vehicle_management_logic" ],
    [ "VehicleNextMaintances", "class_public_transport_1_1_logic_1_1_vehicle_next_maintances.html", "class_public_transport_1_1_logic_1_1_vehicle_next_maintances" ],
    [ "WorkHours", "class_public_transport_1_1_logic_1_1_work_hours.html", "class_public_transport_1_1_logic_1_1_work_hours" ]
];