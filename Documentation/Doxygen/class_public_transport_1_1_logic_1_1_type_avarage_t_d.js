var class_public_transport_1_1_logic_1_1_type_avarage_t_d =
[
    [ "Equals", "class_public_transport_1_1_logic_1_1_type_avarage_t_d.html#a420c8949fd399e60513fb46481efff1d", null ],
    [ "GetHashCode", "class_public_transport_1_1_logic_1_1_type_avarage_t_d.html#a97eaeb56edc296f6b1f42e6ee3b59f5c", null ],
    [ "ToString", "class_public_transport_1_1_logic_1_1_type_avarage_t_d.html#a49e4e8e6f10191fe2592e14624c42b85", null ],
    [ "AvarageTravelDistance", "class_public_transport_1_1_logic_1_1_type_avarage_t_d.html#a726f5287e60234b270d15e84172a0167", null ],
    [ "Type", "class_public_transport_1_1_logic_1_1_type_avarage_t_d.html#a63afe9644a6711051e32dbbbafd248ec", null ]
];