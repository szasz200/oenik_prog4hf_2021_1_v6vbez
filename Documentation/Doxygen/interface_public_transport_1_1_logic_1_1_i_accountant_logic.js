var interface_public_transport_1_1_logic_1_1_i_accountant_logic =
[
    [ "GetDieselConsumption", "interface_public_transport_1_1_logic_1_1_i_accountant_logic.html#adaafade94a4618640a6fe4433246de1a", null ],
    [ "GetElectricityConsumption", "interface_public_transport_1_1_logic_1_1_i_accountant_logic.html#aad6e262f45942e3a174dbcd4d711f8d6", null ],
    [ "GetEmployeeWorkingDates", "interface_public_transport_1_1_logic_1_1_i_accountant_logic.html#aa0f053a6e15bee1010e4aceb28364653", null ],
    [ "GetEmployeeWorkingMinutes", "interface_public_transport_1_1_logic_1_1_i_accountant_logic.html#a30729d9b83212bc3b37c4d60f82bb976", null ],
    [ "GetMaintenances", "interface_public_transport_1_1_logic_1_1_i_accountant_logic.html#a5f975eee9e485e612656f755186546a5", null ],
    [ "GetVehicleCosts", "interface_public_transport_1_1_logic_1_1_i_accountant_logic.html#af8709e8c380178e11c9a321d89262356", null ],
    [ "GetWageEmployee", "interface_public_transport_1_1_logic_1_1_i_accountant_logic.html#a6ecbf470c3c6d45513e73021e44289ae", null ],
    [ "GetWorkingDates", "interface_public_transport_1_1_logic_1_1_i_accountant_logic.html#a442c4e65d4a018905683e6c48187300f", null ],
    [ "NewWorkDate", "interface_public_transport_1_1_logic_1_1_i_accountant_logic.html#a6ebd1d6939b55d0d8d22ed1076f0c0f6", null ]
];