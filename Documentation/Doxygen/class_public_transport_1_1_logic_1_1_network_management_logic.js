var class_public_transport_1_1_logic_1_1_network_management_logic =
[
    [ "NetworkManagementLogic", "class_public_transport_1_1_logic_1_1_network_management_logic.html#aada9ae4a3cc529ccfacc3eafcbddc10a", null ],
    [ "AvarageLineTypeLengths", "class_public_transport_1_1_logic_1_1_network_management_logic.html#a5b59607e3e9e82cdf8e8f655d9b0ccda", null ],
    [ "AvarageLineTypeLengthsAsync", "class_public_transport_1_1_logic_1_1_network_management_logic.html#a38786f40043a5b3d410ee3bbe2bd3456", null ],
    [ "ChangeLength", "class_public_transport_1_1_logic_1_1_network_management_logic.html#a13839ea88ebdb95255b9d5b543167429", null ],
    [ "ChangeNumberOfStops", "class_public_transport_1_1_logic_1_1_network_management_logic.html#a634db1248b856811b5c78f220d6093d8", null ],
    [ "ChangeNumberOfVehicles", "class_public_transport_1_1_logic_1_1_network_management_logic.html#a8a6fc97793348eea6a3a7a2cacc73545", null ],
    [ "ChangeTimeRequired", "class_public_transport_1_1_logic_1_1_network_management_logic.html#aea75a704e59905d34aaa147f5c5be021", null ],
    [ "GetLine", "class_public_transport_1_1_logic_1_1_network_management_logic.html#a451b7ee78c281e05f64ef6de8451e923", null ],
    [ "GetLines", "class_public_transport_1_1_logic_1_1_network_management_logic.html#a08ec0b341827e940b027527b6d35a251", null ],
    [ "GetTypeCounts", "class_public_transport_1_1_logic_1_1_network_management_logic.html#ae3892578ce3516c2475926dfc1f0c3c7", null ],
    [ "LengthOfNetwork", "class_public_transport_1_1_logic_1_1_network_management_logic.html#a91bf62716a59829e71b645a73a9ddeb6", null ],
    [ "NewLine", "class_public_transport_1_1_logic_1_1_network_management_logic.html#af0678735e24c44ba8741c6a94f91a5c3", null ]
];