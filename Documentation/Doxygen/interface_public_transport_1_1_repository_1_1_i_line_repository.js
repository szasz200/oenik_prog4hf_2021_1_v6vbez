var interface_public_transport_1_1_repository_1_1_i_line_repository =
[
    [ "UpdateLength", "interface_public_transport_1_1_repository_1_1_i_line_repository.html#ac552d7a0e8701cc9a2954500d8c69684", null ],
    [ "UpdateNumberOfStops", "interface_public_transport_1_1_repository_1_1_i_line_repository.html#af318e2d482aaef9a2d57e0af5deb8082", null ],
    [ "UpdateNumberOfVehicles", "interface_public_transport_1_1_repository_1_1_i_line_repository.html#a3e8fe6489256160c1a75e49b99d208cf", null ],
    [ "UpdateTimeRequired", "interface_public_transport_1_1_repository_1_1_i_line_repository.html#adae85324b04d1e36471acd41019deec1", null ],
    [ "UpdateType", "interface_public_transport_1_1_repository_1_1_i_line_repository.html#a6cd562e4ffa2ff437b9a38fd9a1e2b0e", null ]
];