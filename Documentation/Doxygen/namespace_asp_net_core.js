var namespace_asp_net_core =
[
    [ "Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", "class_asp_net_core_1_1_views_____view_imports" ],
    [ "Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", "class_asp_net_core_1_1_views_____view_start" ],
    [ "Views_Employees_EmployeesDetails", "class_asp_net_core_1_1_views___employees___employees_details.html", "class_asp_net_core_1_1_views___employees___employees_details" ],
    [ "Views_Employees_EmployeesEdit", "class_asp_net_core_1_1_views___employees___employees_edit.html", "class_asp_net_core_1_1_views___employees___employees_edit" ],
    [ "Views_Employees_EmployeesIndex", "class_asp_net_core_1_1_views___employees___employees_index.html", "class_asp_net_core_1_1_views___employees___employees_index" ],
    [ "Views_Employees_EmployeesList", "class_asp_net_core_1_1_views___employees___employees_list.html", "class_asp_net_core_1_1_views___employees___employees_list" ],
    [ "Views_Employees_EmployeesNew", "class_asp_net_core_1_1_views___employees___employees_new.html", "class_asp_net_core_1_1_views___employees___employees_new" ],
    [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
    [ "Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", "class_asp_net_core_1_1_views___home___privacy" ],
    [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ],
    [ "Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial" ],
    [ "Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", "class_asp_net_core_1_1_views___shared___error" ]
];