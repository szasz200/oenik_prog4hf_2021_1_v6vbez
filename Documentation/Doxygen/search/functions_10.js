var searchData=
[
  ['vehicle_506',['Vehicle',['../class_public_transport_1_1_data_1_1_models_1_1_vehicle.html#a48e0404b26f850efe71d2ab96765e150',1,'PublicTransport::Data::Models::Vehicle']]],
  ['vehiclemanagementlogic_507',['VehicleManagementLogic',['../class_public_transport_1_1_logic_1_1_vehicle_management_logic.html#ae809b116e9820e8d5741b43b651e88bd',1,'PublicTransport::Logic::VehicleManagementLogic']]],
  ['vehiclerepository_508',['VehicleRepository',['../class_public_transport_1_1_repository_1_1_vehicle_repository.html#aef6180b3a67539c6e51ad82a32254948',1,'PublicTransport::Repository::VehicleRepository']]],
  ['vehicles_509',['Vehicles',['../interface_public_transport_1_1_logic_1_1_i_vehicle_management_logic.html#a7086833068d7e47dd5590ae77b047f08',1,'PublicTransport.Logic.IVehicleManagementLogic.Vehicles()'],['../class_public_transport_1_1_logic_1_1_vehicle_management_logic.html#a718f65fa9c7dfc299bc49347d87ecd1f',1,'PublicTransport.Logic.VehicleManagementLogic.Vehicles()']]]
];
