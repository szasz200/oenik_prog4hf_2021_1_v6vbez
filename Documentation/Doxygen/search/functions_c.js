var searchData=
[
  ['remove_482',['Remove',['../class_public_transport_1_1_m_v_c_1_1_controllers_1_1_employees_controller.html#a8585139438b8d2429c29403c6f036baf',1,'PublicTransport::MVC::Controllers::EmployeesController']]],
  ['removeemployee_483',['RemoveEmployee',['../class_public_transport_1_1_logic_1_1_human_resource_logic.html#aa95b1e037eb9d215361635246a9c37fe',1,'PublicTransport.Logic.HumanResourceLogic.RemoveEmployee()'],['../interface_public_transport_1_1_logic_1_1_i_human_resource_logic.html#a4279f4e60111425a97b110cd1cf6df66',1,'PublicTransport.Logic.IHumanResourceLogic.RemoveEmployee()']]],
  ['removevehicle_484',['RemoveVehicle',['../interface_public_transport_1_1_logic_1_1_i_vehicle_management_logic.html#a4eadc41d31d78647e508b398de5fa7ce',1,'PublicTransport.Logic.IVehicleManagementLogic.RemoveVehicle()'],['../class_public_transport_1_1_logic_1_1_vehicle_management_logic.html#a73a3274819a1d7427efedaeaa487c81e',1,'PublicTransport.Logic.VehicleManagementLogic.RemoveVehicle()'],['../class_public_transport_1_1_logic_1_1_test_1_1_vehicle_manangement_logic_test.html#ac01e192b149c91c0cdd36879afc22e97',1,'PublicTransport.Logic.Test.VehicleManangementLogicTest.RemoveVehicle()']]],
  ['repositories_485',['Repositories',['../class_public_transport_1_1_repository_1_1_repositories.html#aa2111e88e51a74cd478602c80927e1b1',1,'PublicTransport::Repository::Repositories']]]
];
