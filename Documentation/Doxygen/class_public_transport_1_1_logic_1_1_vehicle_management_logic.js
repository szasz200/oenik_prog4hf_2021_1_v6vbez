var class_public_transport_1_1_logic_1_1_vehicle_management_logic =
[
    [ "VehicleManagementLogic", "class_public_transport_1_1_logic_1_1_vehicle_management_logic.html#ae809b116e9820e8d5741b43b651e88bd", null ],
    [ "ChangeNextMaintenance", "class_public_transport_1_1_logic_1_1_vehicle_management_logic.html#a791d685828e567fb8a5ea1cbdae65ef7", null ],
    [ "GetAvarageTypeTDistances", "class_public_transport_1_1_logic_1_1_vehicle_management_logic.html#a16f677a135b08cae1cc1da898c951b37", null ],
    [ "GetAvarageVehicleTypeAge", "class_public_transport_1_1_logic_1_1_vehicle_management_logic.html#ae74c00b16979e434091fba08352b84ab", null ],
    [ "GetMaintenances", "class_public_transport_1_1_logic_1_1_vehicle_management_logic.html#ade89f53db82b6a360ef9bb0552ba6b0c", null ],
    [ "GetNextMaintances", "class_public_transport_1_1_logic_1_1_vehicle_management_logic.html#a19e988456b97de84022ca6abb126df35", null ],
    [ "GetTodayMaintenances", "class_public_transport_1_1_logic_1_1_vehicle_management_logic.html#a2af761b6c04120348ae8b55470061552", null ],
    [ "GetVehicle", "class_public_transport_1_1_logic_1_1_vehicle_management_logic.html#a93a78da4b54a9edbe0e577ae37d96062", null ],
    [ "NewMaintenance", "class_public_transport_1_1_logic_1_1_vehicle_management_logic.html#aa5f6ea9c283ebbaa2edf7d1e43492569", null ],
    [ "NewVehicle", "class_public_transport_1_1_logic_1_1_vehicle_management_logic.html#aeae619f77c7af1a4d3b8efb0c5d3702e", null ],
    [ "RemoveVehicle", "class_public_transport_1_1_logic_1_1_vehicle_management_logic.html#a73a3274819a1d7427efedaeaa487c81e", null ],
    [ "Vehicles", "class_public_transport_1_1_logic_1_1_vehicle_management_logic.html#a718f65fa9c7dfc299bc49347d87ecd1f", null ]
];